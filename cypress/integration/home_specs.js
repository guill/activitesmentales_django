describe('Home Test', () => {
    it('Check authentification', () => {
        cy.visit('/')
        cy.contains('Connexion').click()
        cy.get('#id_username').type('User1')
        cy.get('#id_password').type('test')
        cy.get('[data-testid=valider]')
            .click()
        cy.get('[data-testid=username]').should('contain', 'User1')

    })
})