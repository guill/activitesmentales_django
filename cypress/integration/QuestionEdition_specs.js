/// <reference types="cypress" />
describe('test Question edition', () => {
    beforeEach(() => {
        cy.login("User1", "test")
    });


    // test editing existing / new / duplicate question
    // check theme visibility
    // check updates after saving : theme visible / not visible,


    // new question
    it('Question creation', () => {
        cy.visit('/')
            .get('[data-testid=theme_2').click()
            .get('[data-testid=theme_21 ]').click()

        cy.get('[data-testid=newQuestion]')
            .click()

        // test empty code => error
        cy.get('[data-testid=question-editor]')
            .get('[data-testid=valider')
            .click()
            .get('[data-testid=code-error]')

            // test empty themes list => error
            .get('#code')
            .type('Test $3^1$')
            .get('[data-testid=valider')
            .click()
            .get('[data-testid=themes-error]')

            // compilation error
            .get('#code')
            .type('Test $3^1')
            .get('[data-testid=question-editor')
            .find('[data-testid=theme_2]').click()
            .get('[data-testid=question-editor')
            .find('[data-testid=theme_21]').click()
            .get('[data-testid=question-editor')
            .find('[data-testid=theme_28]').click()
            .get('[data-testid=valider]').click()
            .get('[data-testid=valider').click()
            .get('[data-testid=code-error]')
            .get('[data-testid=latexlog]')

            // validation : ok
            .get('#code').clear()
            .type('Test $3^1$')

        // test if the question is visible in the QuestionList
        cy.intercept('POST', '/api/question/').as('question')
        cy.get('[data-testid=valider').click()
        cy.wait('@question').then((interception) => {
            let id = interception.response.body.id
            cy.get('[data-testid=question_' + id + ']').should('be.visible')
        })
    })

    //
    // // show the not empty any more theme
    // cy.get('[data-testid=theme_18]').click()
    //
    // // edit again
    // cy.get('.atrier').dblclick()
    // })

    it('Question edition', () => {
        cy.visit('/')
            .get('[data-testid=theme_2').click()
            .get('[data-testid=theme_21]').click()
            .get('[data-testid=question_10] img')
            .dblclick()

        cy.get('[data-testid=question-editor')
            .find('#avalider').should('not.be.enabled')


    })

    // // duplicate question
    // it('Question duplicate', () => {
    //     cy.visit('/')
    //         .get('[data-testid=theme_1').click()
    //         .get('[data-testid=theme_11 ]').click()
    //         .get('[data-testid=newQuestion]')
    //         .click()
    // })

// in an empty theme
// delete
})