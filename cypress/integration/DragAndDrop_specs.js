describe('test Drag&Drop', () => {
    beforeEach(() => {
        cy.login("User1", "test")
    });

    it('Check theme visibility & order', () => {
        let dataTransfer = new DataTransfer()
        cy.visit('/')
            .get('[data-testid=theme_2').click()
            .get('[data-testid=theme_21 ]').click()
            .get('[data-testid=question_14]')
            .trigger('dragstart', {dataTransfer})

        // add a question to a seance
        cy.get('[data-testid=slot_10_2]').trigger('drop', {dataTransfer})
            .find('[alt="question 14"]').should('exist')
        //TODO: check the score

        // remove a question
        dataTransfer = new DataTransfer()
        cy.get('[data-testid=slot_10_2]').find('[alt="question 14"]')
            .trigger('dragstart', {dataTransfer})
        cy.get('[data-testid=question_11]')
            .trigger('drop', {dataTransfer})
        cy.get('[data-testid=slot_10_2]')
            .find('[alt="question 14"]').should('not.exist')


    })
})
