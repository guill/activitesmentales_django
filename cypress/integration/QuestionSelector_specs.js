describe('test QuestionSelector', () => {
    beforeEach(
        () => cy.login("User1", "test")
    );

    it('Check theme visibility & order', () => {
        cy.visit('/')
        // //Check the order
        // cy.get('.theme').eq(1).should('contain.text', 'Thème 1')
        // cy.get('.theme').eq(2).should('contain.text', 'Inter Thèmes')
        // cy.get('.theme').eq(3).should('contain.text', 'Thème 2')

        cy.contains('Thème 2')
            .click()
        // Theme without questions
        cy.contains('Sous-thème vide').should('not.exist')
        cy.contains('Sous-thème 2.1').should('exist')

        // theme with question but not for this user
        cy.contains('Sous-thème 3.1').should('not.exist')
        // theme with question but not for this user, validated
        cy.contains('Sous-thème 3.2').should('exist')


    })

    // Check question visibility

    //Check question editability
    it('Check if question is editable', () => {
        cy.visit('/')
            .get('[data-testid=theme_2').click()
            .get('[data-testid=theme_21').click()

        // question1 : unordered, unused, not 'a valider'
        cy.get('[data-testid=question_21]')
            // .should('have.class', 'unordered')
            // editable
            .dblclick().get('[data-testid=question-editor]').get('[data-testid=cancel').click()

        // question2: ordered, unused,  'a valider' => éditable
        cy.get('[data-testid=theme_12').click()
            .get('[data-testid=question_12]')
            .dblclick().get('[data-testid=question-editor]').get('[data-testid=cancel').click()

        // question3 : a valider, used => not editable
        cy.get('[data-testid=question_13]')
            .dblclick().get('[data-testid=question-editor]').should('not.exist')

        // question4 : validée : not editable
        cy.get('[data-testid=theme_21').click()
            .get('[data-testid=question_14]')
            .dblclick().get('[data-testid=question-editor]').should('not.exist')

        // tooltip : show ; follow a link

        // has_question : question dans un thème mais non visible pour l'utilisateur
    })
})