// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add('login', (user, pw) => {
    cy.request({
        method: "GET",
        url: "/logout/"
    });
    return cy.request({
        url: '/login/',
        method: 'GET' // cookies are in the HTTP headers, so HEAD suffices
    }).then(() => {
        cy.getCookie('sessionid').should('not.exist')
        cy.getCookie('csrftoken').its('value').then((token) => {
            let oldToken = token
            cy.request({
                url: '/login/',
                method: 'POST',
                form: true,
                followRedirect: false, // no need to retrieve the page after login
                body: {
                    username: user,
                    password: pw,
                    csrfmiddlewaretoken: token
                }
            }).then(() => {
                cy.getCookie('sessionid').should('exist')
                return cy.getCookie('csrftoken').its('value')

            })
        })
    })

    // cy.visit("/login/");
    // cy.get("[name=csrfmiddlewaretoken]")
    //     .should("exist")
    //     .should("have.attr", "value")
    //     .as("csrfToken");
    //
    // cy.get("@csrfToken").then((token) => {
    //     cy.request({
    //         method: "POST",
    //         url: "/login/",
    //         form: true,
    //         body: {
    //             username: user,
    //             password: pw,
    //         },
    //         headers: {
    //             "X-CSRFTOKEN": token,
    //         },
    //     });
    // });
    //
    // cy.getCookie("sessionid").should("exist");
    // cy.getCookie("csrftoken").should("exist");

    Cypress.Cookies.preserveOnce("sessionid", "csrftoken");

})

