Django==4.0.5
djangorestframework==3.13.1
djangorestframework-recursive==0.1.2
pdf2image==1.16.0
