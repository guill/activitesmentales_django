INSERT INTO am_theme (id, label, parent_id, position)
SELECT id, label, parentId, position
FROM themes;
INSERT INTO am_question (id, code, auteur_id, validee, avalider, thumb_name)
SELECT id, code, 2, false, true, printf("question_%06d.png", id)
FROM exos;
INSERT INTO am_classe (id, nom, nom_latex, annee, enseignant_id)
SELECT id, label, latexLabel, anneeDebut, 4
FROM classes;
INSERT INTO am_feuille (id, classe_id, position)
select id, classeId, position
from feuilles;
INSERT INTO am_seance (id, feuille_id, date, position)
SELECT id, feuilleId, date, position
from seances;
INSERT INTO am_questionposee (seance_id, position + 1, question_id, score)
SELECT seanceId, position, exoId, score
FROM lienExosSeances;
INSERT INTO am_latexmodel (id, label, description, type, position, delete_tmp)
SELECT id, label, description, upper(substr(type, 1, 2)), rang, deleteTmp
from latexModels;
INSERT INTO am_latexmodelpart (type, code, model_id)
SELECT upper(substr(type, 1, 2)), content, latexModelId
from latexModelParts;

insert into am_questionorder(question_id, theme_id, position)
select exoId, themeId, ifnull(position, 0)
from lienExosThemes
         join exos on exoId = exos.id;