sftp am@$SERVER <<EOT
cd /var/www/activitesmentales
put -r activitesmentales
put -r am
put -r templates
put -r public
put webpack-stats.json
EOT
