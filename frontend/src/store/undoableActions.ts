import {IFeuille, IQuestionPosee, ISeance, ISeanceR, IUndoable, SeanceID} from "@/types";
import {useFeuillesStore} from "@/store/feuilles";
import {useClassesStore} from "@/store/classes";

abstract class BaseUndoable implements IUndoable {
    protected _done: boolean
    public redoMessage: string
    public undoMessage: string

    protected constructor(done = false, redoMessage = '', undoMessage = '') {
        this._done = done
        this.redoMessage = redoMessage
        this.undoMessage = undoMessage
    }

    get done() {
        return this._done
    }

    set done(done) {
        this._done = done
    }

    abstract do(): Promise<any>
}

export class MultipleAction extends BaseUndoable {
    protected actions: (IUndoable | IUndoable[])[] // the actions that are in the same array can be simultaneously

    constructor(redoMessage: string = '', undoMessage: string = '', actions: (IUndoable | IUndoable[])[] = []) {
        super(false, redoMessage, undoMessage)
        this.actions = actions
    }

    async do() {
        if (this.done) {
            for (let i = this.actions.length - 1; i >= 0; i--) {
                const action = this.actions[i]
                if (Array.isArray(action)) {
                    let requests = []
                    for (let item of action)
                        requests.push(item.do())
                    await Promise.all(requests)
                } else
                    await action.do()
            }
        } else {
            for (let action of this.actions) {
                if (Array.isArray(action)) {
                    let requests = []
                    for (let item of action)
                        requests.push(item.do())
                    await Promise.all(requests)
                } else
                    await action.do()
            }
        }
        this.done = ! this.done
        console.log(this.redoMessage + ' done', this.done)
    }
}


export class QuestionPoseeAddDelAction extends BaseUndoable {
        private questionPosee: Partial<IQuestionPosee>

    constructor(questionPosee: Partial<IQuestionPosee>, add: boolean) {
        const questionId = questionPosee.question
        super(!add,'Ajouter la question ' + questionId, 'Retirer la question' + questionId)
        this.questionPosee = questionPosee
    }

    setSeanceId(seanceId: SeanceID) {
            this.questionPosee.seance = seanceId
    }

     do() {
        console.debug('QuestionPoseeAddDelAction.do', this.questionPosee, this.done)
        const feuilles = useFeuillesStore()
        if (this._done) {
            this._done = false
            return feuilles._deleteQuestionPosee(this.questionPosee as IQuestionPosee)
                .then(() => {
                    let promises = []
                    promises.push(feuilles.loadSeance(this.questionPosee.seance!, true))
                    promises.push(feuilles.loadScoresOfQuestion(this.questionPosee.question!))
                })
        } else {
            this._done = true
            return feuilles._addQuestionPosee(this.questionPosee)
                .then((questionPosee) => {
                    if (! this.questionPosee.id) {
                        this.questionPosee.id = questionPosee.id
                        return questionPosee
                    }
                    feuilles.loadSeance(this.questionPosee.seance!, true)
                })
        }
    }
}

export class QuestionPoseePatchAction extends BaseUndoable {
    private questionPosee: IQuestionPosee
    private newValues: Partial<IQuestionPosee>

    constructor(questionPosee: IQuestionPosee, newValues: Partial<IQuestionPosee>) {
        super()
        if (newValues.question && questionPosee.question !== newValues.question)
            this.redoMessage = 'Changer de question'
        if ((newValues.position && questionPosee.position !== newValues.position) ||
            (newValues.seance && questionPosee.seance !== newValues.seance))
            this.redoMessage = 'Déplacer la question'
        this.undoMessage = this.redoMessage
        this.questionPosee = questionPosee
        this.newValues = newValues
    }

    do() {
        console.debug('QuestionPoseePatchAction.do', this.questionPosee, this.newValues, this.done)
        const feuilles = useFeuillesStore()
        return feuilles._patchQuestionPosee(this.questionPosee, this.newValues)
            .then(() => {
                for (const key of Object.keys(this.newValues)) {
                    // @ts-ignore
                    const newValue = this.newValues[key]
                    // @ts-ignore
                    this.newValues[key] = this.questionPosee[key]
                    // @ts-ignore
                    this.questionPosee[key] = newValue
                }
            })
    }
}


class _SeanceAddDelAction extends BaseUndoable {
    private seance: Partial<ISeanceR>

    constructor(seance: Partial<ISeanceR>, add: boolean) {
        super(!add)
        this.seance = seance
    }

    getId() { return this.seance.id }

    do() {
        console.debug('_SeanceAddDelAction.do', this.seance, this.done)
        const feuilles = useFeuillesStore()
        if (this.done) {
            this.done = false
            return feuilles._deleteSeance(this.seance as ISeanceR)
        } else {
            this.done = true
            return feuilles._addSeance(this.seance)
                .then((seance: ISeance) => {
                    if (! this.seance.id) {
                        this.seance.id = seance.id
                    }
                })
        }
    }
}

class _SeanceAddDelActionR extends MultipleAction {
    private seance?: Partial<ISeanceR>
    private add?: boolean

    constructor(seance: Partial<ISeanceR>, add: boolean = true, redoMsg?: string, undoMsg?: string) {
        if (seance.feuille === undefined) {
            console.error('new SeanceAddDelActionR: position is undef', seance, add)
            super()
            return
        }
        const feuilles = useFeuillesStore()
        let questions = []
        if (! seance.position) {
            let position = 0
            const feuille =  feuilles.get(seance.feuille)
            for (const seanceTmpId of feuille.seances) {
                const seanceTmp = feuilles.getSeance(seanceTmpId)
                if (seanceTmp && seanceTmp.position > position)
                    position = seanceTmp.position
            }
            seance.position = position + 1
        }
        if (seance.questionsPosees) {
            for (const questionPosee of seance.questionsPosees) {
                questions.push(new QuestionPoseeAddDelAction(questionPosee, add))
            }
        }
        let actions
        if (add) {
            actions = [new _SeanceAddDelAction(seance, add), questions]
            if (! redoMsg)
                redoMsg = 'Ajouter la séance ' + seance.position
            if (! undoMsg)
                undoMsg = 'Supprimer la séance ' + seance.position
        } else {
            actions = [questions, new _SeanceAddDelAction(seance, add)]
            if (! redoMsg)
                redoMsg = 'Supprimer la séance ' + seance.position
            if (! undoMsg)
                undoMsg = 'Restaurer la séance ' + seance.position
        }
        super(redoMsg, undoMsg, actions)
        this.seance = seance
        this.add = add
    }

    async do() {
        console.debug('SeanceAddDelActionR.do', this.seance, this.done)
        const feuilles = useFeuillesStore()
        if (this.done || ! this.add)
            await super.do()
        else { // new seance added
            const seanceAction = this.actions[0] as _SeanceAddDelAction
            await seanceAction.do()
            const requests = []
            const questionActions = this.actions[1] as QuestionPoseeAddDelAction[]
            for (let qa of questionActions) {
                qa.setSeanceId(seanceAction.getId()!)
                requests.push(qa.do())
            }
            await Promise.all(requests)
            this.done = !this.done
        }
        console.log(this.redoMessage + ' done', this.done)
        await feuilles.loadFeuille(this.seance?.feuille!, true)
    }
}

export class SeanceAddActionR extends _SeanceAddDelActionR {
    constructor(seance: Partial<ISeanceR>, redoMsg?: string, undoMsg?: string) {
        super(seance, true, redoMsg, undoMsg);
    }
}

export class SeancesAddActionR extends MultipleAction {
    constructor(seances: Partial<ISeanceR>[], redoMsg?: string, undoMsg?: string) {
        const actions = []
        for (const seance of seances)
            actions.push(new SeanceAddActionR(seance))
        if (! redoMsg)
            redoMsg = seances.length > 1 ? 'Insérer des séances' : 'Insérer la séance'
        if (! undoMsg)
            undoMsg = seances.length > 1 ? 'Supprimer des séances' : 'Supprimer la séance'
        super(redoMsg, undoMsg, actions);
    }
}

export class SeanceDelActionR extends _SeanceAddDelActionR {
    constructor(seance: Partial<ISeanceR>, redoMsg?: string, undoMsg?: string) {
        super(seance, false, redoMsg, undoMsg);
    }
}

export class SeancesDelActionR extends MultipleAction {
    constructor(seances: ISeanceR[], redoMsg?: string, undoMsg?: string) {
        const actions = []
        for (const seance of seances)
            actions.push(new SeanceDelActionR(seance))
        if (redoMsg === undefined)
            redoMsg = seances.length>1 ? 'Supprimer les séances' : 'Supprimer la séance'
        if (undoMsg === undefined)
            undoMsg = seances.length>1 ? 'Restaurer les séances' : 'Restaurer la séance'
        super(redoMsg, undoMsg, actions);
    }
}

/* **********************
 *      Feuilles        *
 ************************/

class _FeuilleAddDelAction extends BaseUndoable {
        private feuille: Partial<IFeuille>

    constructor(feuille: Partial<IFeuille>, add: boolean) {
        super(! add,'Supprimer une feuille', 'Ajouter une feuille')
        this.feuille = feuille
    }

     do() {
        console.debug('_FeuilleAddDelActionR.do', this.feuille, this.done)
        const feuilles = useFeuillesStore()
        if (this._done) {
            this._done = false
            return feuilles._deleteFeuille(this.feuille as IFeuille)
        } else {
            this._done = true
            return feuilles._addFeuille(this.feuille)
                .then((feuille) => {
                    if (!this.feuille.hasOwnProperty('id')) {
                        this.feuille.id = feuille.id
                    }
                })
        }
    }
}

export class FeuilleAddDelActionR extends MultipleAction {
    private feuille: Partial<IFeuille>

    constructor(feuille: Partial<IFeuille>, add: boolean = true) {
        if (feuille.classe === undefined) {
            console.error('new FeuilleAddDelActionR with classe undef', feuille, add)
            super()
            this.feuille = feuille
        }
        const feuilles = useFeuillesStore()
        const classes = useClassesStore()
        let seancesActions = []

        if (feuille.seances === undefined)
            feuille.seances = []
        for (const seanceId of feuille.seances) {
            const seance = feuilles.getSeance(seanceId)
            if (seance)
                seancesActions.push(new _SeanceAddDelActionR(seance, add))
        }
        let actions: ( IUndoable | IUndoable[] )[] = [seancesActions, new _FeuilleAddDelAction(feuille, add)]
        let isInsertion = false
        if (feuille.position === undefined) {
            let newPos = 0
            const classe = classes.get(feuille.classe!)
            for (const feuilleTmpId of classe.feuilles) {
                const feuilleTmp = feuilles.get(feuilleTmpId)
                if (feuilleTmp.position > newPos)
                    newPos = feuilleTmp.position
            }
            feuille.position = newPos + 1
        }
        if (add)
            super((isInsertion ? 'Insérer' : 'Ajouter') + ' la feuille ' + feuille.position, 'Supprimer la feuille ' + feuille.position, actions)
        else
            super('Supprimer la feuille ' + feuille.position, 'Restaurer la feuille ' + feuille.position, actions)

        this.feuille = feuille
    }

    do() {
        console.debug('FeuilleAddDelActionR.do', this.feuille, this.done)
        const classes = useClassesStore()
        const feuilles = useFeuillesStore()
        return super.do().then(() => {
            feuilles.loadFeuilles()
            classes.load(this.feuille.classe!, true)
        })
    }
}

export class FeuillePatchAction extends BaseUndoable {
    private feuille: IFeuille
    private newValues: Partial<IFeuille>

    constructor(feuille: IFeuille, newValues: Partial<IFeuille>) {
        super()
        if (newValues.position && feuille.position !== newValues.position)
            this.redoMessage = 'Deplacer la feuille'
        this.undoMessage = this.redoMessage
        this.feuille = feuille
        this.newValues = newValues
    }

    do() {
        console.debug('FeuillePatchAction.do', this.feuille, this.newValues, this.done)
        const feuilles = useFeuillesStore()
        return feuilles._patchFeuille(this.feuille.id, this.newValues)
            .then(() => {
                for (const key of Object.keys(this.newValues)) {
                    // @ts-ignore
                    const newValue = this.newValues[key]
                    // @ts-ignore
                    this.newValues[key] = this.feuille[key]
                    // @ts-ignore
                    this.feuille[key] = newValue
                }
            })
    }
}