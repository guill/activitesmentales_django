/*
 * Copyright (c) 2022.
 * GuiΩ guillaume.claus@worldonline.fr
 */

import axios from 'axios'

export const $axios = axios
$axios.defaults.xsrfHeaderName = 'X-CSRFToken';
$axios.defaults.xsrfCookieName = 'csrftoken';
