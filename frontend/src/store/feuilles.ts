/*
 * Copyright (c) 2022.
 * GuiΩ guillaume.claus@worldonline.fr
 */
import {defineStore} from 'pinia'
import type {
    ClasseID,
    FeuilleID,
    IFeuille,
    IQuestionPosee,
    IScore,
    ISeance,
    ISeanceR,
    QuestionID,
    SeanceID
} from "@/types";
import {useClassesStore} from "@/store/classes";
import {useThemesStore} from "@/store/themes";
import {$axios} from "@/store/axios-instance";
// import $axios from "@/axios-instance";


interface IFeuillesStore {
    _feuilles: Map<FeuilleID, IFeuille>,
    _seances: Map<SeanceID, ISeanceR>,
    _scores: Map<ClasseID, Map<QuestionID, IScore[]>>,
    _loadingSeancesForClasse: Map<ClasseID, Promise<any>>,
}


export const useFeuillesStore = defineStore('feuillesStore',{
    state: (): IFeuillesStore => ({
         // the already loaded sheets
        _feuilles: new Map<FeuilleID, IFeuille>(),

         // the already loaded sessions
        _seances: new Map<SeanceID, ISeanceR>(),

        // all the scores, grouped by question, class
        // scores[classeID][questionID]
        _scores: new Map<ClasseID, Map<QuestionID, IScore[]>>(),

        // Promises to test if seances for classe is loaded
        _loadingSeancesForClasse: new Map<ClasseID, Promise<any>>()
    }),

    getters: {
        /**
         * @returns the list of all the sheets for the current classe
         */
        getCurrFeuilles: (state: IFeuillesStore) => {
            const classes = useClassesStore()
            if (classes.hasSelection)
                return classes.getSelected.feuilles
            else
                return null
        },

        /**
         * @param feuilleId
         * @returns the sheet of the given id
         */
        get: (state: IFeuillesStore) =>  (feuilleId: FeuilleID) =>
            state._feuilles.get(feuilleId) as IFeuille,

        /**
         *
         * @param questionId
         * @returns the list of the scores for the given question and the current class
         */
        getScores: (state: IFeuillesStore) =>  {
            const classes = useClassesStore()
            const scoresPerQ = state._scores.get(classes.getSelectedId as ClasseID) as Map<QuestionID, IScore[]>
            return (questionId: QuestionID) =>  {
                const scores = scoresPerQ.get(questionId)
                if (scores)
                    return scores
                else
                    return null
            }
        },
        isSeancesLoadingForClasse(state:IFeuillesStore) { return (classeId: ClasseID) => {
            const promise = this._loadingSeancesForClasse.get(classeId)
            return promise ? promise : Promise.resolve()
        }},


        /**
         * @param seanceId
         * @returns the session of the given id
         */
        getSeance: (state: IFeuillesStore) => (seanceId: SeanceID) => {
            const seance = state._seances.get(seanceId)
            if (seance)
                return seance
            else
                return null
        },
    },

    actions: {
        initScore() {
            const classes = useClassesStore()
            const classeId = classes.getSelectedId as ClasseID
            if (!this._scores.has(classeId))
                this._scores.set(classeId, new Map<QuestionID, IScore[]>)
        },


        /************************
         *      Feuilles        *
         ************************/

        /**
         * load the sheet of the given id and its sessions
         * @param feuilleID
         * @returns {Promise<void>}
         */
        loadFeuille(feuilleId: FeuilleID, force = false) {
            console.debug('feuilles.loadFeuille', feuilleId, force)
            if (this._feuilles.has(feuilleId) && !force)
                return Promise.resolve()
            return $axios.get("api/feuille/" + feuilleId + '/')
                .then((response: { data: IFeuille }) => {
                    const newFeuille = response.data
                    this._feuilles.set(newFeuille.id, newFeuille);
                    for (const seanceId of newFeuille.seances) {
                        this.loadSeance(seanceId, force)
                    }
                })
                .catch(error => {
                    console.error(error)
                })
        },

        /**
         * load all the sheets for the current class
         * @returns {Promise<void>}
         */
        loadFeuilles() {
            console.debug('feuilles.loadFeuilles')
            const classes = useClassesStore()
            const classeId = classes.getSelectedId
            if (classeId) {
                const promise = $axios.get("api/feuille/?classe_id=" + classeId)
                    .then(response => {
                        for (const feuille of response.data)
                            this._feuilles.set(feuille.id, feuille);
                    })
                    .catch(error => console.error('feuilles.loadFeuilles', error));
                return promise
            }
            return Promise.resolve()
        },

        /**
         * Add a new empty sheet to the current class
         * @param commit
         * @returns {Promise<void>}
         */
        _addFeuille(feuille: Partial<IFeuille>) {
            console.debug('feuilles._addFeuille', feuille)
            const classes = useClassesStore()
            const feuilles = useFeuillesStore()
            let request
            if (feuille.id)
                request = $axios.put("api/feuille/" + feuille.id + '/', feuille)
            else
                request = $axios.post("api/feuille/", feuille)

            return request.then(response => {
                const feuille = response.data
                this._feuilles.set(feuille.id, feuille)
                return feuille
            })
                .catch(error => {
                    console.log(error)
                })
        },

        _deleteFeuille(feuille: IFeuille) {
            console.debug('feuilles._deleteFeuille', feuille)
            const classes = useClassesStore()
            return $axios.delete('api/feuille/' + feuille.id + '/')
                .then(() => {
                    const classe = classes.get(feuille.classe)
                    const index = classe.feuilles.indexOf(feuille.id)
                    classe.feuilles.splice(index, 1)
                    this._feuilles.delete(feuille.id)
                })
                .catch(error => console.log(error.response))
        },

        _patchFeuille(feuilleId: FeuilleID, newValues: Partial<IFeuille>) {
            console.debug('feuilles.patchFeuille', feuilleId, newValues)
            const feuilles = useFeuillesStore()
            return $axios.patch('api/feuille/' + feuilleId + '/', newValues)
                .then((response) => {
                    const feuille = response.data
                    console.log('feuilles._patchFeuille', feuille)
                    this._feuilles.set(feuille.id, feuille)
                })
                .catch(error => console.log(error.response.data))
        },


        /********************
         *      Seances     *
         ********************/

        _addSeance(seance: Partial<ISeanceR>) {
            console.debug('feuilles._addSeance', seance)
            let request
            if (seance.hasOwnProperty('id'))
                request = $axios.put('api/seance/' + seance.id + '/', seance)
            else
                request = $axios.post("api/seance/", seance)
            return request
                .then(response => {
                    const newSeance = response.data
                    this._seances.set(newSeance.id, newSeance)
                    return newSeance
                })
                .catch(error => console.log(error.data))
        },

        _deleteSeance(seance: ISeanceR) {
            console.debug('feuilles._deleteSeance', seance)
            return $axios.delete('api/seance/' + seance.id + '/')
                .then(() => {
                    const feuille = this.get(seance.feuille)
                    const index = feuille.seances.indexOf(seance.id)
                    feuille.seances.splice(index, 1)
                    this._seances.delete(seance.id)
                })
                .catch(error => console.log(error.data))
        },

        loadSeance(seanceId: SeanceID, force = false) {
            console.debug('feuilles.LoadSeance', seanceId)
            // console.trace()
            const promise = $axios.get('api/seancer/' + seanceId + '/')
                .then((response: { data: ISeanceR }) => {
                    const seance = response.data
                    this._seances.set(seance.id, seance)
                })
                .catch(error => console.log(error))
            return promise
        },

        loadSeances() {
            console.debug('feuilles.LoadSeances')
            const classes = useClassesStore()
            const classeId = classes.getSelectedId
            let promise = Promise.resolve()
            if (classeId) {
                promise = $axios.get('api/seancer/?classe_id=' + classeId)
                    .then(response => {
                        for (const seance of response.data)
                            this._seances.set(seance.id, seance)
                    })
                    .catch(error => console.log(error))
                this._loadingSeancesForClasse.set(classeId, promise)
            }
            return promise
        },


        /**
         *
         * @param commit
         * @param state
         * @param data = {seanceID: number ,newValue: object}
         * @returns {Promise<void>}
         */
        patchSeance(seanceId: SeanceID, newValues: Partial<ISeance>) {
            console.debug('feuilles.patchSeance', seanceId, newValues)
            const feuilles = useFeuillesStore()
            return $axios.patch('api/seance/' + seanceId + '/', newValues)
                .then(() => feuilles.loadSeance(seanceId, true))
                .catch(error => console.log(error.response.data))
        },


        /****************************
         *      QuestionPosees      *
         ****************************/

        /**
         * private action
         * @param data = {seance, position, question}
         * @returns Promise<QuestionPosee>
         * @private
         */
        _addQuestionPosee(questionPosee: Partial<IQuestionPosee>) {
            console.debug('feuilles._addQuestionPosee', questionPosee)
            const feuilles = useFeuillesStore()
            let request
            if (questionPosee.hasOwnProperty('id') && questionPosee.id)
                request = $axios.put("api/question-posee/" + questionPosee.id + '/', questionPosee)
            else
                request = $axios.post("api/question-posee/", questionPosee)
            return request
                .then(response => {
                    let promises = []
                    promises.push(feuilles.loadSeance(questionPosee.seance!))
                    // promises.push(this.dispatch('questions/loadQuestion', questionPosee.question))
                    promises.push(feuilles.loadScoresOfQuestion(questionPosee.question!))
                    return Promise.all(promises).then(() => {
                        return response.data
                    })
                })
                .catch(error => console.error(error.response))
        },

        _deleteQuestionPosee(questionPosee: IQuestionPosee) {
            console.debug('feuilles._deleteQuestionPosee', questionPosee)
            return $axios.delete('api/question-posee/' + questionPosee.id + '/')
                .then(response => {
                    const seance = this.getSeance(questionPosee.seance)!
                    const index = seance.questionsPosees.indexOf(questionPosee)
                    seance.questionsPosees.splice(index, 1)
                    this._seances.delete(questionPosee.id)
                })
                .catch(error => console.log(error, error.response))
        },

        _patchQuestionPosee(questionPosee: IQuestionPosee, newValues: Partial<IQuestionPosee>) {
            console.debug('feuilles._patchQuestionPosee', questionPosee, newValues)
            return $axios.patch('api/question-posee/' + questionPosee.id + '/', newValues)
                .then(() => {
                    if (newValues.hasOwnProperty('seance')
                        && newValues.seance !== questionPosee.seance) {
                        this.loadSeance(newValues.seance!)
                    }
                    this.loadSeance(questionPosee.seance!)
                    if (newValues.hasOwnProperty('question')
                        && newValues.question !== questionPosee.question) {
                        this.loadScoresOfQuestion(newValues.question!)
                    }
                    this.loadScoresOfQuestion(questionPosee.question)
                })
                .catch(error => console.log(error))
        },


        /********************
         *      Scores      *
         ********************/

        loadScoresOfQuestion(questionId: QuestionID) {
            const themes = useThemesStore()
            const classes = useClassesStore()
            if (!themes.hasSelection || !classes.hasSelection)
                return
            return $axios.get("api/question-posee/?theme_id=" + themes.getSelectedId
                + "&classe_id=" + classes.getSelectedId
                + "&question_id=" + questionId)
                .then(response => {
                    const questionPosees = response.data
                    let scores = []
                    for (let questionPosee of questionPosees) {
                        scores.push(questionPosee.score);
                    }
                    (this._scores.get(classes.getSelectedId as ClasseID) as Map<QuestionID, IScore[]>)
                        .set(questionId, scores)
                })
        },
    }
})
