import {defineStore} from 'pinia'
import type {ClasseID, FeuilleID, IFeuille, IQuestionPosee, ISeanceR, IUndoable, QuestionID, SeanceID} from "@/types";
import {useClassesStore} from "@/store/classes";
import {
    FeuilleAddDelActionR,
    MultipleAction,
    QuestionPoseeAddDelAction,
    QuestionPoseePatchAction,
    SeanceAddActionR,
    SeanceDelActionR, SeancesAddActionR, SeancesDelActionR
} from "@/store/undoableActions";
import {useFeuillesStore} from "@/store/feuilles";


export const QUESTION_SAVE_INIT = 'INIT_QUESTION_SAVE'

interface IUndoablesStore {
    queues: Map<ClasseID, IUndoable[]>,
    next: Map<ClasseID, number>
}

export const useUndoablesStore = defineStore( 'undoablesStore',{
    state: (): IUndoablesStore => ({
        // one undoables queue for each classe
        queues: new Map<ClasseID, IUndoable[]>(),

        // index of the next action for each classe
        next: new Map<ClasseID, number>()
    }),

    getters: {
        hasRedoable: (state) => {
            const classes = useClassesStore()
            if (classes.getSelectedId) {
                const next = state.next.get(classes.getSelectedId) as number
                const queue = state.queues.get(classes.getSelectedId) as IUndoable[]
                return next < queue.length
            }
            else
                return false
        },

        hasUndoable: (state) => {
            const classes = useClassesStore()
            if (classes.getSelectedId) {
                const next = state.next.get(classes.getSelectedId) as number
                return next > 0
            } else
                return false
        },

        getUndoable(): IUndoable {
            const classes = useClassesStore()
            const classeId = classes.getSelectedId as number
            return (this.queues.get(classeId) as IUndoable[])[this.next.get(classeId) as number - 1]
        },

        getRedoable(): IUndoable {
            const classes = useClassesStore()
            const classeId = classes.getSelectedId as number
            return (this.queues.get(classeId) as IUndoable[])[this.next.get(classeId) as number]
        },

        getRedoMessage(): string {
            if (this.hasRedoable) {
                const action = this.getRedoable
                if (action.done) {
                    return action.undoMessage
                } else {
                    return action.redoMessage
                }
            } else {
                return ''
            }
        },

        getUndoMessage(): string {
            if (this.hasUndoable) {
                let action = this.getUndoable
                if (action.done) {
                    return action.undoMessage
                } else {
                    return action.redoMessage
                }
            } else {
                return ''
            }
        },
    },

    actions: {
        initUndoQueue(classeId: ClasseID) {
           if (! this.queues.has(classeId)) {
               this.queues.set(classeId, [])
               this.next.set(classeId, 0)
           }
        },

        incNext(inc = 1) {
            const classes = useClassesStore()
            const classeId = classes.getSelectedId as number
            const next = this.next.get(classeId) as number
            this.next.set(classeId, next + inc)
        },

        redo() {
            const action = this.getRedoable
            this.incNext()
            return action.do()
        },

        undo() {
            this.incNext(-1)
            let action = this.getRedoable
            return action.do()
        },

        queuePush(action: IUndoable) {
            const classes = useClassesStore()
            const classeId = classes.getSelectedId as ClasseID
            (this.queues.get(classeId) as IUndoable[]).splice(this.next.get(classeId) as number)
            this.queues.get(classeId)!.push(action)
        },

        /*******************
         *      Feuilles
         *******************/

        addFeuille(classeId: ClasseID, position?: number) {
            console.debug('undoables.addFeuille', classeId, position)
            this.queuePush(new FeuilleAddDelActionR({
                classe: classeId,
                position: position,
            }, true))
            return this.redo()
        },

        deleteFeuille(feuille: IFeuille) {
            console.debug('undoables.deleteFeuille', feuille)
            this.queuePush(new FeuilleAddDelActionR(feuille, false))
            return this.redo()
        },


        /* ******************
         *      Seances     *
         ********************/

        addSeances(seances: Partial<ISeanceR>[], redoMsg?: string, undoMsg?: string) {
            for (const seance of seances)
                if (seance.questionsPosees === undefined)
                    seance.questionsPosees = []
            this.queuePush(new SeancesAddActionR(seances, redoMsg, undoMsg))
            return this.redo()
        },

        addSeance(seance: Partial<ISeanceR>, redoMsg?: string, undoMsg?: string) {
            console.debug('undoables.addSeance', seance)
            if (seance.questionsPosees === undefined)
                seance.questionsPosees = []
            this.queuePush(new SeanceAddActionR(seance, redoMsg, undoMsg))
            return this.redo()
        },

        deleteSeance(seance: ISeanceR) {
            console.debug('undoables.deleteSeance', seance)
            this.queuePush(new SeanceDelActionR(seance))
            return this.redo()
        },

        deleteSeances(seancesId: SeanceID[]) {
            const feuilles = useFeuillesStore()
            console.debug('undoables.deleteSeances', seancesId)
            const seances = []
            for (const seanceId of seancesId)
                seances.push(feuilles.getSeance(seanceId)!)
            this.queuePush(new SeancesDelActionR(seances))
            return this.redo()
        },


        /* ********************
         *   QuestionPosees
         **********************/

        addQuestionPosee(questionPosee: Partial<IQuestionPosee>) {
            console.debug('undoables.addQuestionPosee', questionPosee)
            this.queuePush(new QuestionPoseeAddDelAction(questionPosee, true))
            return this.redo()
        },

        deleteQuestionPosee(questionPosee: IQuestionPosee) {
            console.debug('undoables.deleteQuestionPosee', questionPosee)
            this.queuePush(new QuestionPoseeAddDelAction(questionPosee, false))
            return this.redo()
        },

        patchQuestionPosee(questionPosee: IQuestionPosee, newValues: Partial<IQuestionPosee>) {
            console.debug('undoables.patchQuestionPosee', questionPosee, newValues)
            this.queuePush(new QuestionPoseePatchAction(questionPosee, newValues))
            return this.redo()
        },

        swapQuestionPosee(questionPosee1: IQuestionPosee, questionPosee2: IQuestionPosee) {
            console.debug('undoables.swapQuestionPosee', questionPosee1, questionPosee2)
            this.queuePush(new MultipleAction('Permutter les questions', 'Permutter les questions',
                    [
                        new QuestionPoseePatchAction(questionPosee1, {question: questionPosee2.question}),
                        new QuestionPoseePatchAction(questionPosee2, {question: questionPosee1.question})
                    ]))
            return this.redo()
        },

        replaceQuestionPosee(source: IQuestionPosee | QuestionID, questionPoseeDest: IQuestionPosee) { // replace 2 by 1
            console.debug('undoables.replaceQuestionPosee', source, questionPoseeDest)
            let actions = []
            // if (source instanceof IQuestionPosee) // source is a QuestionPosee => to delete
            let question: QuestionID
            if (typeof source === 'number') {
                question = source
            } else {// source is a QuestionPosee => to delete
                actions.push(new QuestionPoseeAddDelAction(source, false))
                question = source.question
            }
            actions.push(new QuestionPoseePatchAction(questionPoseeDest, {question: question}))
            this.queuePush(new MultipleAction('Remplacer la question', 'Remplacer la question', actions))
            this.redo()
        },

        setScores(data: {questionPosee: IQuestionPosee, score: number | null}[]) {
            console.debug('undoables.setScores', data)
            let actions = []
            for (const item of data)
                actions.push(new QuestionPoseePatchAction(item.questionPosee, {score: item.score}))
            this.queuePush(new MultipleAction('Modifier les scores', 'Restaurer les scores', [actions]))
            return this.redo()

        }

    },
})


