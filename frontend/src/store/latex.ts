/*
 * Copyright (c) 2022.
 * GuiΩ guillaume.claus@worldonline.fr
 */

import {defineStore} from 'pinia'
import {$axios} from "@/store/axios-instance";
import {IModel, IModelPart, IScore, ModelID, ModelPartID, ModelType} from "@/types";

interface ILatexStore {
    _models: Map<ModelID, IModel>,
    _parts: Map<ModelPartID, IModelPart>,
    _promiseModels: Promise<any> | null,
    _promiseParts: Promise<any> | null,
    _parttypes: Record<ModelType, ModelType[]>
}

export const useLatexStore = defineStore('latexStore',{
    state: (): ILatexStore => ({
        _models: new Map<ModelID, IModel>(),
        _parts: new Map<ModelPartID, IModelPart>(),
        _promiseModels: null,
        _promiseParts: null,
        _parttypes: {'HE': ['HE'], 'QU': ['QU'], 'SE': ['SE', 'QU'], 'FE': ['FE', 'SE', 'QU'], 'FI': ['FI']},
    }),

    getters: {
        getModel: (state: ILatexStore) => (modelId: ModelID) => {
            return state._models.get(modelId) as IModel
        },

        getAllModels: (state: ILatexStore) => {
            return Array.from(state._models.values())
        },

        getPartTypes: (state: ILatexStore) => (modelType: ModelType) => {
            return state._parttypes[modelType]
        }
    },

    actions: {
        loadModels() {
            let promise = $axios.get('api/latexmodel/')
                .then((response: { data: IModel[]}) => {
                    for (const model of response.data)
                        this._models.set(model.id!, model)
                })
                .catch(error => console.error(error))
            this._promiseModels = promise
            return promise
        },

        async waitforModel(modelId: ModelID) {
            if (this._promiseModels)
                await this._promiseModels
            return this._models.get(modelId) as IModel
        },

        async saveModel(model: Partial<IModel>, parts: IModelPart[]) {
            if (!model.id) {
                const response = await $axios.post('api/latexmodel/', model)
                model = response.data
            } else {
                for (let partId of this.getModel(model.id!).parts) {
                    if (!parts.find(part => part.id === partId))
                        await $axios.delete('api/latexmodelpart/' + partId + '/')
                }
            }
            model.parts = []
            let promises = []
            for (let part of parts) {
                part.model = model.id!
                console.log(part)
                promises.push(this.savePart(part).then(part => model.parts!.push(part.id)))
            }
            await Promise.all(promises)
            const response = await $axios.patch('api/latexmodel/' + model.id + '/', model)
            const newModel = response.data
            this._models.set(newModel.id!, newModel)
            return newModel
        },

        async loadParts() {
            let promise = $axios.get('api/latexmodelpart/')
                .then(response => {
                    for (let part of response.data)
                        this._parts.set(part.id!, part)
                })
            this._promiseParts = promise
            return promise
        },

        getPart(partId: ModelPartID) {
            if (this._promiseParts)
                return this._promiseParts.then(() => {
                    return this._parts.get(partId) as IModelPart
                })
            else
                return this._parts.get(partId) as IModelPart
        },

        async savePart(part: IModelPart) {
            console.log('savePart', part)
            let response
            if (part.id)
                response = await $axios.patch('api/latexmodelpart/' + part.id + '/', part)
            else  // part.id==null => create new part
                response = await $axios.post('api/latexmodelpart/', part)
            const newPart = response.data
            this._parts.set(newPart.id, newPart)
            return newPart
        }

    }
})
// const mutations = {
//     'MODEL'(state, model) {
//         Vue.set(state.models, model.id, model)
//     },
//     'PART'(state, part) {
//         Vue.set(state.parts, part.id, part)
//     },
//     'PROMISE_MODELS'(state, promise) {
//         state.promiseModels = promise
//     },
//     'PROMISE_PARTS'(state, promise) {
//         state.promiseParts = promise
//     }
// }
