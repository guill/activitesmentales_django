import {defineStore} from 'pinia'
import {ClasseID, IClasse} from "@/types"
import {useFeuillesStore} from "@/store/feuilles";
import {useQuestionsStore} from "@/store/questions";
import {$axios} from "@/store/axios-instance";
import {useUndoablesStore} from "@/store/undoables";

// import $axios from "axios";

// $axios.defaults.xsrfHeaderName = 'X-CSRFToken';
// $axios.defaults.xsrfCookieName = 'csrftoken';

interface IClassesStore {
    _annees: number[],
    _selectedAnnee: number | undefined,
    _classes: Map<ClasseID, IClasse>,
    _currIds: ClasseID[],
    _selectedId: ClasseID | undefined,
}

export const useClassesStore = defineStore('classesStore', {
    state: (): IClassesStore => ({
         // year for which the current user has classes
        _annees: [],
        // selected year
        _selectedAnnee: undefined,
        // map of already loaded classes
        _classes: new Map<ClasseID, IClasse>(),
        // ids of the classes for the selected year
        _currIds: [],
        // id of the selected classe
        _selectedId: undefined,
    }),

    getters: {

        // Annees
        getAnnees: (state: IClassesStore) => state._annees,
        getSelectedAnnee: (state: IClassesStore) => state._selectedAnnee,

        isLoaded: (state: IClassesStore) => (classeID: ClasseID) => {
            return state._classes.has(classeID)
        },

        get: (state: IClassesStore) => (classeId: ClasseID) => {
            return state._classes.get(classeId) as IClasse
        },

        getAllClasses: (state: IClassesStore) => {
            return Array.from(state._classes.values())
        },

        hasSelection: (state: IClassesStore) => { return state._selectedId !== undefined },
        getSelectedId: (state: IClassesStore) => state._selectedId,
        getSelected: (state: IClassesStore): IClasse => state._classes.get(state._selectedId!) as IClasse,
        getCurrIds: (state: IClassesStore) => state._currIds,

    },

    actions: {
        setSelectedAnnee(annee: number) {
            this._selectedAnnee = annee
            return this.loadCurrClasses()
        },

        /**
         * Loads and sets the list of the years of the classes for the actual user
         * @returns {Promise<void>}
         */
        loadAnnees() {
            return $axios.get("api/annee_list/")
                .then((response: { data: number[] }) => {
                    this._annees = response.data
                    return this.setSelectedAnnee(response.data[response.data.length - 1])
                })
                .catch((error: any) => console.error(error));
        },

        /**
         * Set the current class
         * and load the joined data
         * @param classeId
         * @return Promise
         */
        setSelectedId(classeId: ClasseID) {
            const feuilles = useFeuillesStore()
            const questions = useQuestionsStore()
            const undoables = useUndoablesStore()
            this._selectedId = classeId
            feuilles.initScore()
            undoables.initUndoQueue(classeId)
            let promises = []
            promises.push(feuilles.loadFeuilles())
            promises.push(feuilles.loadSeances())
            promises.push(questions.loadQuestionsOfSelectedClasse())
            return Promise.all(promises)
        },

        /**
         * Loads and sets the list of the classes for the currently selected year
         * @returns {Promise<void>}
         */
        load(classeId: ClasseID, force: boolean = false) {
            console.debug('classes.load', classeId, force)
            return $axios.get('api/classe/' + classeId)
                .then((response) => {
                    const classe = response.data
                    this._classes.set(classe.id, classe)
                })
                .catch((error: any) => console.error(error));
        },

        /**
         * Loads and sets the list of the classes for the currently selected year
         * @returns {Promise<void>}
         */
        loadCurrClasses() {
            console.debug('classes.loadCurrClasses')
            if (this._selectedAnnee) {
                return $axios.get("api/classe/?annee=" + this._selectedAnnee)
                    .then((response: {data: IClasse[]}) => {
                        this._currIds.length = 0
                        for (const classe of response.data) {
                            this._currIds.push(classe.id!)
                            this._classes.set(classe.id!, classe)
                        }
                        this.setSelectedId(this._currIds[0])
                    })
                    .catch((error: any) => console.error(error));
            } else {
                this._currIds = []
                this._selectedId = undefined
                return Promise.resolve()
            }
        },

        /**
         * Load all the classes for the current user
         * @returns {Promise<void>}
         */
        loadAllClasses() {
            console.debug('classes.loadAllClasses')
            return $axios.get('api/classe/')
                .then((response: {data: IClasse[]}) => {
                    let ids = []
                    for (const classe of response.data) {
                        ids.push(classe.id)
                        this._classes.set(classe.id!, classe);
                    }
                })
                .catch((error: any) => console.error(error));
        },

        /**
         * Save/Update a class in the database
         * @param classe
         * @returns {Promise<void>}
         */
        saveClasse(classe: IClasse) {
            // TODO: check unicity(enseignant,annee, nom) before the exception raises
            let request
            if (classe.id)
                request = $axios.put('api/classe/' + classe.id + '/', classe)
            else
                request = $axios.post('api/classe/', classe)
            return request
                .then((response: { data: IClasse }) => {
                    const newClasse = response.data
                    this._classes.set(newClasse.id!, newClasse)
                    const classes = useClassesStore()
                    classes.loadAllClasses()
                    if (newClasse.annee === this._selectedAnnee)
                        classes.loadCurrClasses()
                })
                // TODO : check line below
                // .catch(error => this.$emit('message', error.response.data))
        },
    },
})

// const getters = {
// }
//
// const mutations = {
//     /**
//      * Set the list of the years for the classes of the current user
//      * @param annees
//      */
//     'ANNEES'(state, annees) {
//         state._annees = annees
//     },
//
//     /**
//      * Set the currently selected year
//      * @param annee
//      */
//     'CURR_ANNEE'(state, annee) {
//         state.curr_annee = annee;
//     },
//
//     /**
//      * Set the id of the currently selected class
//      * @param classe_id
//      */
//     'CURR_CLASSE_ID'(state, classe_id) {
//         state.curr_classe_id = parseInt(classe_id)
//         this.commit('undoables/INIT_UNDO_QUEUE', classe_id, {root: true})
//     },
//
//     /**
//      * Add a class in the list of the loaded classes
//      * @param classe
//      */
//     'CLASSE'(state, classe) {
//         Vue.set(state._classes, classe.id, classe)
//     },
//
//     /**
//      * Set the list of the id of the classes for the currently selected year
//      * @param classe_ids
//      */
//     'CURR_CLASSE_IDS'(state, classe_ids) {
//         state.curr_classe_ids = classe_ids
//     },
// }