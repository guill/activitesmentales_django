/*
 * Copyright (c) 2022.
 * GuiΩ guillaume.claus@worldonline.fr
 */
import {defineStore} from 'pinia'
import axios from 'axios'
import {IQuestion, QuestionID, ThemeID} from "@/types";
import {useThemesStore} from "@/store/themes";
import {useClassesStore} from "@/store/classes";
import {useFeuillesStore} from "@/store/feuilles";
import {$axios} from "@/store/axios-instance";

// import $axios from "@/axios-instance";

export interface IQuestionsStore {
    _questions: Map<QuestionID, IQuestion>,
    _questionsAValider: QuestionID[],
    // TODO: remove any
    _questionSavedStatus: {saved: boolean, errors: any},
    _isLoading: boolean
}

export const useQuestionsStore = defineStore('questionsStore',{
    state: (): IQuestionsStore => ({
        _questions: new Map<QuestionID, IQuestion>(),
        _questionsAValider: [],
        _questionSavedStatus: {saved: false, errors: {}},
        _isLoading: false

    }),

    getters: {
        isLoading: (state: IQuestionsStore) => state._isLoading,

        /**
         * @returns the list of the ids of the questions for the current theme
         */
        getForCurrTheme: (state: IQuestionsStore) => {
            const themes = useThemesStore()
            if (! themes.hasSelection)
                return []
            const theme = themes.getSelected
            return theme?.questions
        },

        get: (state: IQuestionsStore) => (questionId: QuestionID) =>
            state._questions.get(questionId) as IQuestion,

        // Questions:
        getQuestionsOfTheme: (state: IQuestionsStore) => (themeId: ThemeID) => {
            const themes = useThemesStore()
            return themes.get(themeId).questions
        },

        getIndexInTheme: (state: IQuestionsStore) => (questionId: QuestionID, themeId: ThemeID) => {
            console.debug('questions.getIndexInTheme', questionId, themeId)
            const themes = useThemesStore()
            return themes.get(themeId).questions!.findIndex((question) => question === questionId)
        },

        getPositionInTheme() {
            return (question: IQuestion, themeId: ThemeID) => {
                const themePos = question.themes.find((themePos) => themePos.theme === themeId)
                console.debug('questions.getPositioninTheme', question, themeId, ' =>', themePos ? themePos.position : themePos)
                return themePos ? themePos.position : themePos
            }
        },

        getSaveErrors: (state: IQuestionsStore) => state._questionSavedStatus.errors,

        getSaveStatus: (state: IQuestionsStore) =>  state._questionSavedStatus.saved,

        getAValider: (state: IQuestionsStore) =>  state._questionsAValider,


    },

    actions: {
        initSave() {
            this._questionSavedStatus = {saved: false, errors: {}}
        },

        saveQuestion(oldQuestion: IQuestion, newQuestion:IQuestion) {
            let request
            if (newQuestion.id) {
                request = $axios.patch('api/question/' + newQuestion.id + '/', newQuestion)
            } else {
                request = $axios.post('api/question/', newQuestion)
            }
            return request
                .then(response => {
                    const question = response.data
                    this._questions.set(question.id, question)
                    this._questionSavedStatus.saved = true
                    this.questionChanged(oldQuestion, question)
                })
                .catch(error => this._questionSavedStatus.errors = error.response.data)
        },

        // data = {oldQuestion:..., newQuestion:...}
        questionChanged(oldQuestion: IQuestion, newQuestion: IQuestion) {
            for (const index in newQuestion.themes) {
                if (newQuestion.themes.hasOwnProperty(index)) {
                    const themePos = newQuestion.themes[index];
                    if (!oldQuestion || this.getPositionInTheme(oldQuestion, themePos.theme) !== themePos.position)
                        this.loadQuestionsOfTheme(themePos.theme)
                }
            }
            if (oldQuestion) {
                for (const themePos of oldQuestion.themes) {
                    this.loadQuestionsOfTheme(themePos.theme);
                }
            }
        },

        changeQuestionPosition(questionId: QuestionID, newValues: {theme_id: ThemeID, position: number}) {
            const themes = useThemesStore()
            return $axios.patch('api/question/' + questionId + '/change_position/', newValues)
                .then(response => {
                    const question = response.data
                    this._questions.set(question.id, question)
                    this.loadQuestionsOfTheme(themes.getSelectedId!)
                })
                .catch(error => console.log(error.data))
        },

        loadQuestion(questionId: QuestionID) {
            return axios.get('api/question/' + questionId + '/')
                .then(response => {
                    const question = response.data
                    this._questions.set(question.id, question)
                })
                .catch(error => console.log(error.response))
        },

        loadQuestions(questionIds: QuestionID[]) {
            if (questionIds.length === 0)
                return
            this._isLoading = true
            return axios.get("api/question/?" + questionIds.map(id => 'ids[]=' + id).join('&'))
                .then(response => {
                    for (let question of response.data) {
                        this._questions.set(question.id, question)
                    }
                })
                .catch(error => console.error(error))
                .finally(() => this._isLoading = false)
        },

        loadQuestionsOfSelectedClasse() {
            const classes = useClassesStore()
            if (classes.hasSelection) {
                return axios.get("api/question/?classe_id=" + classes.getSelectedId)
                    .then(response => {
                        for (let question of response.data) {
                            this._questions.set(question.id, question)
                        }
                    })
                    .catch(error => console.error(error))
            }
        },

        loadQuestionsOfTheme(themeId: ThemeID): Promise<void> {
            return axios.get('api/theme/' + themeId + '/question_list/')
                .then((response: { data: QuestionID[] }) => {
                    const questionIds = response.data
                    const themes = useThemesStore()
                    themes.setQuestionsOfTheme(themeId, questionIds)
                    let ids = []
                    const feuilles = useFeuillesStore()
                    for (const questionId of questionIds) {
                        feuilles.loadScoresOfQuestion(questionId)
                        if (!this._questions.has(questionId))
                            ids.push(questionId)
                    }
                    this.loadQuestions(ids)
                })
                .catch(error => console.log(error))
        },

        loadQuestionsAValider() {
             return axios.get('api/question/get_avalider/')
                .then((response) => {
                    this._questionsAValider = response.data
                    this.loadQuestions(this._questionsAValider)
                })
                .catch(error => console.log(error))
        },

        startEdition() {
            this._questionSavedStatus = {saved: false, errors: {}}
        },
    }
})

