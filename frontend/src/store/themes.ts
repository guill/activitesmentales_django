import {defineStore} from 'pinia'
import type {ClasseID, IQuestionPosee, ITheme, QuestionID, ThemeID} from "@/types";
import {useFeuillesStore} from "@/store/feuilles";
import {useQuestionsStore} from "@/store/questions";
import {$axios} from "@/store/axios-instance";


interface IThemeStore {
    _themes: Map<ThemeID, ITheme>,
    _selectedId: ThemeID | undefined,
    _selectedIds: ThemeID[],
    _evolution: IQuestionPosee[],
    _isLoading: boolean,
    _isLoadingEvolution: boolean,
}

export const useThemesStore = defineStore('themesStore',{
    state: (): IThemeStore => ({
        _themes: new Map<ThemeID, ITheme>(),
        _selectedId: undefined,
        _selectedIds: [],
        _evolution: [],
        _isLoading: false,
        _isLoadingEvolution: false,
    }),

    getters: {
        isLoading: (state: IThemeStore) => state._isLoading,

        /**
         * @param themeId
         * @returns the theme for the given id
         */
        get: (state: IThemeStore) => (themeId: ThemeID) => state._themes.get(themeId) as ITheme,
        getSelectedId: (state:IThemeStore) => state._selectedId,
        getSelected: (state: IThemeStore) => state._themes.get(state._selectedId as ThemeID) as ITheme,
        isLoadingEvolution: (state: IThemeStore) => state._isLoadingEvolution,
        getEvolution: (state:IThemeStore) => state._evolution,
        hasSelection:(state: IThemeStore) => state._selectedId !== undefined
    },

    actions: {
        setQuestionsOfTheme(themeId: ThemeID, questions: QuestionID[]) {
            const theme = this.get(themeId)
            theme.questions = questions
            theme.hasQuestion = questions.length > 0
            this._themes.set(theme.id, theme)
        },

        setSelectedTheme(themeId: ThemeID) {
            const feuilles = useFeuillesStore()
            if (themeId === this._selectedId)
                return
            this._selectedId = themeId
            feuilles.initScore()
            const theme = this.get(themeId)
            if (theme.questions  === undefined) {
                const questions = useQuestionsStore()
                return questions.loadQuestionsOfTheme(themeId)
            }
            return Promise.resolve()
        },

        getNextPosition(themeId:ThemeID, position: number, before= false) {
            return $axios.get("api/theme/" + themeId + '/get_next_position/?position=' + position + (before ? '&before':''))
                .then(response => response.data)
                .catch(error => console.error(error));
        },

        /**
         * Loads the theme of the given themeId
         * @param themeId
         */
        loadTheme(themeId: ThemeID, force = false) {
            if (! force && this._themes.has(themeId))
                return
            this._isLoading = true
            return $axios.get('api/theme/' + themeId + '/')
                .then(response => {
                    const theme = response.data
                    this._themes.set(theme.id, theme)
                })
                .catch(error => console.error(error))
                .finally(() => this._isLoading = false)
        },

        loadThemes() {
            this._isLoading = true
            return $axios.get("api/theme/")
                .then(response => {
                    for (let theme of response.data)
                        this._themes.set(theme.id, theme)
                    for (let theme of response.data)
                        if (theme.enfants.length === 0) {
                            const value = theme.hasQuestion
                            let parent = theme
                            while (parent.parent) {
                                parent = this._themes.get(parent.parent)
                                if (parent.hasQuestion === true ||
                                    parent.hasQuestion === value)
                                    break
                                let newTheme = JSON.parse(JSON.stringify(parent))
                                newTheme.hasQuestion = value
                                this._themes.set(newTheme.id, newTheme)
                            }
                        }
                })
                .catch(error => console.error(error))
                .finally(() => this._isLoading = false)
        },

        loadEvolution(themeId: ThemeID, classeId: ClasseID) {
            this._isLoadingEvolution = true
            this._evolution = []
            return $axios.get("api/theme/" + themeId + '/get_evolution/?classe_id=' + classeId)
                .then(response => this._evolution = response.data)
                .catch(error => console.error(error))
                .finally(() => this._isLoadingEvolution = false)
        },
    }
})