import Router, {createRouter, createWebHistory} from "vue-router";
import MainPage from '@/components/MainPage.vue'
import Configuration from "@/components/Configuration.vue";
import Documentation from "@/components/Documentation.vue";

const routes = [
    {
      path: '/am',
      // component: Configuration,
      component: MainPage,
      name: 'MainPage'
    },
    {
      path: '/am/documentation',
      component: Documentation,
      name: 'Documentation'
    },
    {
      path: '/am/configuration',
      component: Configuration,
      name: 'Configuration'
    },
    // {
    //   path: '*',
    //   redirect: '',
    // }
]

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;
