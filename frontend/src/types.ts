declare global {
    var username: string;
    var permissions: string[];
}

export type ClasseID = number

export interface IClasse {
    id: ClasseID,
    nom: string,
    nom_latex: string,
    annee: number,
    feuilles: FeuilleID[],
}

export interface IClipboardContent {
    seances: IClipboardSeance[]
}

export interface IClipboardQuestionPosee {
    position: number,
    question: QuestionID
}

export interface IClipboardSeance {
    feuille: FeuilleID,
    questionsPosees: IClipboardQuestionPosee[]
}

export interface IContextMenuOption {
    name?: string,
    type?: 'item' | 'submenu' | 'rule',
    callback?: (e?: MouseEvent) => void,
    disabled?: boolean,
    hidden?: boolean,
    // options?: IContextMenuOption
  }

export interface IQuestionDropEvent {
    event: DragEvent,
    seanceSourceId?: SeanceID,
    positionSource?: number,
    questionPoseeSourceId?: QuestionPoseeID,
    questionSourceId: QuestionID
    scoreSource?: number,
    seanceDest?: ISeanceR,
    positionDest?: number,
    questionPoseeDest?: IQuestionPosee,
    questionDestId?: QuestionID
}

export type FeuilleID = number

export interface IFeuille {
    id: FeuilleID,
    classe: ClasseID,
    position: number,
    seances: SeanceID[]
}

export type ModelID = number

// HEader, FEuille, SEance, QUestion, FIgure
export type ModelType = 'HE' | 'FE' | 'SE' | 'QU' | 'FI'

export interface IModel {
    id: ModelID,
    label: string,
    description: string,
    file_name: string,
    position: number,
    type: ModelType,
    parts: ModelPartID[],
    delete_tmp: boolean
}

export type ModelPartID = number

export interface IModelPart {
    id: ModelPartID,
    code: string,
    type: ModelType,
    model: ModelID
}

export type QuestionID = number

export interface IThemePos {
    theme: ThemeID,
    position: number | null
}

export interface IQuestion {
    id: QuestionID,
    code: string,
    auteur: string,
    avalider: boolean,
    validee: boolean,
    themes: IThemePos[],
    thumb_name: string | null
}

export type QuestionPoseeID = number

export interface IQuestionPosee {
    id: QuestionPoseeID,
    seance: SeanceID,
    position: number,
    question: QuestionID,
    score: number | null
}

//
// export interface IQuestionPoseeAddDelActionConstructor {
//     new(questionPosee: IQuestionPosee, add: boolean): IQuestionPoseeAddDelAction
// }
//
// export interface IQuestionPoseeAddDelAction {
//     done: boolean,
//     redoMessage: string,
//     undoMessage: string,
//     questionPosee: IQuestionPosee
// }

export interface IScore {
    date: string,
    score: number
}
export type SeanceID = number

export interface ISeance {
    id: SeanceID,
    feuille: FeuilleID,
    date: string | null,
    position: number,
    questionsPosees: QuestionPoseeID[]
}

export interface ISeanceR { // recursive
    id: SeanceID,
    feuille: FeuilleID,
    date: string | null,
    position: number,
    questionsPosees: IQuestionPosee[]
}

export interface ISelection {
    seances: SeanceID[]
}

export type ThemeID = number

export interface ITheme {
    id: ThemeID,
    label: string,
    parent: ThemeID | null
    enfants: ThemeID[],
    hasQuestion: boolean
    questions?: QuestionID[]
}

export interface IUndoable {
    done: boolean
    redoMessage: string
    undoMessage: string
    do(): Promise<void>
}


export type UserID = number