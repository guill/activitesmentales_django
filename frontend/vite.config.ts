import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import {fileURLToPath, URL} from "url";


// https://vitejs.dev/config/

export default defineConfig({

  // where index.html is located ; path relative to the location of the config file itself
  root: './',

  // Base public path when served in development or production.
  base: '/static/',
  plugins: [vue()],
  build: {
    outDir: 'dist/',
    manifest: true,
    rollupOptions: {
      input: 'src/main.ts',
    },
  },
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },

});
