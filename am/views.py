from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Max, Q
from django.http import HttpResponse, JsonResponse, FileResponse
from django.urls import reverse_lazy
from django.views import View
from django.views.generic.edit import CreateView, UpdateView
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied, ValidationError
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from am import serializers
from .forms import UtilisateurCreationForm, UtilisateurChangeForm
from .models import Question, Classe, Feuille, Theme, QuestionPosee, Seance, LatexModel, QuestionOrder, \
    LatexModelPart, Utilisateur
from .serializers import SeanceSerializerR, ThemeSerializer, LatexModelSerializer, \
    LatexModelPartSerializer, QuestionPoseeSerializer


def index(_):
    return HttpResponse("Activités mentales")


class SignUpView(CreateView):
    form_class = UtilisateurCreationForm
    success_url = reverse_lazy('am:login')
    template_name = 'registration/signup.html'


class UserChangeView(LoginRequiredMixin, UpdateView):
    login_url = 'am:login'  # if not logged
    model = Utilisateur
    form_class = UtilisateurChangeForm
    success_url = reverse_lazy('am:home')
    template_name = 'registration/profile.html'

    def get_object(self, queryset=None):
        return self.request.user


class AnneeView(View):
    @staticmethod
    def get(request):
        annees = []
        annee = None
        for val in Classe.objects.filter(enseignant=request.user).order_by('annee').values('annee'):
            if val != annee:
                annees.append(val['annee'])
                annee = val
        return JsonResponse(annees, safe=False)


# ViewSet pour lister / editer les classes
# l'action /feuille_add/ ajoute une feuille pour cette classe
# si ?annee=... est passée en paramètre, seules les feuilles de cette années sont renvoyées
class ClasseViewSet(ModelViewSet):
    serializer_class = serializers.ClasseSerializer
    permission_classes = [permissions.IsAuthenticated]
    queryset = Classe.objects.all().order_by('annee')

    @action(detail=True, methods=['post'])
    def feuille_add(self, request, pk):
        classe = self.get_object()
        newPosition = classe.feuilles.aggregate(Max('position'))['position__max']
        if newPosition is None:
            newPosition = 1
        else:
            newPosition += 1
        feuille = Feuille.objects.create(classe=classe, position=newPosition)
        feuille.save()
        seance = Seance.objects.create(feuille=feuille, position=1)
        seance.save()
        return Response(self.serializer_class(classe).data)

    def get_queryset(self):
        params = self.request.query_params
        queryset = self.queryset.filter(enseignant=self.request.user)
        if 'annee' in params:
            return queryset.filter(annee=params['annee'])
        else:
            return queryset

    def perform_create(self, serializer):
        serializer.save(enseignant=self.request.user)


# ViewSet pour modifier les feuilles
# l'action /seance_add/ permet d'ajouter une séance à la feuille concernée
class FeuilleViewSet(ModelViewSet):
    serializer_class = serializers.FeuilleSerializer
    permission_classes = [permissions.IsAuthenticated]
    queryset = Feuille.objects.all()

    def get_queryset(self):
        queryset = Feuille.objects.filter(classe__enseignant=self.request.user)
        params = self.request.query_params
        if 'classe_id' in params:
            return queryset.filter(classe__id=params['classe_id'])
        else:
            return queryset

    def get_object(self):
        if self.request.method == 'PUT':
            feuille = Feuille.objects.filter(id=self.kwargs.get('pk')).first()
            if feuille:
                return feuille
            else:
                return Feuille(id=self.kwargs.get('pk'))
        else:
            return super().get_object()

    def perform_create(self, serializer):
        feuille = serializer.save()
        for feuilleTmp in Feuille.objects.filter(classe=feuille.classe, position__gte=feuille.position).exclude(
                id=feuille.id).all():
            feuilleTmp.position += 1
            feuilleTmp.save()

    def perform_update(self, serializer):
        feuille = serializer.save()
        for feuilleTmp in Feuille.objects.filter(classe=feuille.classe, position__gte=feuille.position).exclude(
                id=feuille.id).all():
            feuilleTmp.position += 1
            feuilleTmp.save()

    def perform_destroy(self, feuille):
        for feuilleTmp in Feuille.objects.filter(classe=feuille.classe, position__gt=feuille.position).all():
            feuilleTmp.position -= 1
            feuilleTmp.save()
        feuille.delete()

    @action(detail=True, methods=['get'])
    def get_pdf(self, request, pk):
        latexModel = LatexModel.objects.get(pk=request.query_params['model_id'])
        feuille = self.get_object()
        result = latexModel.compile(feuille=feuille)
        if not result['status']:
            return HttpResponse(result['latexlog'].decode('utf-8').replace('\n', '<br>'))
        else:
            path = 'static/out/' + result['outfile']
            return FileResponse(open(path, 'rb'), as_attachment=True, content_type='application/pdf',
                                filename=result['outfile'])


class LatexModelViewSet(ModelViewSet):
    queryset = LatexModel.objects.all()
    serializer_class = LatexModelSerializer
    permission_classes = [permissions.IsAuthenticated]


class LatexModelPartViewSet(ModelViewSet):
    queryset = LatexModelPart.objects.all()
    serializer_class = LatexModelPartSerializer
    permission_classes = [permissions.IsAuthenticated]


# ViewSet pour lister / éditer les questions
# si ?theme_id=... est passé en paramètre,
# seules les questions liées à ce thèmes sont renvoyées
class QuestionViewSet(ModelViewSet):
    queryset = Question.objects.all().order_by('questionorder__position')
    serializer_class = serializers.QuestionSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        params = self.request.query_params
        queryset = QuestionViewSet.queryset
        # print('user.has_perm(am.question_valider)', self.request.user.has_perms('am.question_valider'))
        if self.request.user.has_perms('am.question_valider'):
            queryset = queryset.filter(Q(auteur=self.request.user) | Q(avalider=True) | Q(validee=True))
        else:
            queryset = queryset.filter(Q(auteur=self.request.user) | Q(validee=True))
        if 'theme_id' in params:
            queryset = queryset.filter(themes__id=params['theme_id']).order_by('questionorder__position')
        if 'classe_id' in params:
            queryset = queryset.filter(questionsPosees__seance__feuille__classe__id=params['classe_id'])
        if 'ids[]' in params:
            queryset = queryset.filter(id__in=params.getlist('ids[]'))
        return queryset


    @action(detail=False)
    def get_avalider(self, request):
        ids = []
        for question in Question.objects.filter(Q(validee=False) & Q(avalider=True)):
            ids.append(question.id)
        return Response(ids)

    @action(detail=True, methods=['patch'])
    def change_position(self, request, pk):
        question = self.get_object()
        data = request.data
        questionOrder = QuestionOrder.objects.get(question__id=pk, theme__id=data['theme_id'])
        questionOrder.position = data['position']
        questionOrder.save()
        return Response(self.serializer_class(question, context={'request': request}).data)

    def perform_create(self, serializer):
        serializer.save(auteur=self.request.user)

    def perform_update(self, serializer):
        question = self.get_object()
        if self.request.user.has_perm('am.question_valider') \
                or (question.auteur == self.request.user
                    and not question.validee
                    and question.questionsPosees.count() == 0):
            super().perform_update(serializer)
        else:
            raise ValidationError("Modification interdite.")

    def perform_destroy(self, instance):
        if instance.auteur == self.request.user \
                and not instance.validee \
                and instance.questionsPosees.count() == 0:
            super().perform_destroy(instance)
        else:
            raise ValidationError("Suppression interdite.")


class QuestionPoseeViewSet(ModelViewSet):
    serializer_class = serializers.QuestionPoseeSerializer
    permission_classes = [permissions.IsAuthenticated]
    queryset = QuestionPosee.objects.all()

    def get_queryset(self):
        queryset = super().get_queryset().filter(seance__feuille__classe__enseignant=self.request.user).order_by(
            'seance__date', 'seance__id')
        params = self.request.query_params
        if 'question_id' in params:
            queryset = queryset.filter(question__id=params['question_id'])
        if 'theme_id' in params:
            queryset = queryset.filter(question__themes__id=params['theme_id'])
        if 'classe_id' in params:
            queryset = queryset.filter(seance__feuille__classe__id=params['classe_id'])
        return queryset

    def get_object(self):
        if self.request.method == 'PUT':
            questionPosee = QuestionPosee.objects.filter(id=self.kwargs.get('pk')).first()
            if questionPosee:
                return questionPosee
            else:
                return QuestionPosee(id=self.kwargs.get('pk'))
        else:
            return super().get_object()

    def perform_create(self, serializer):
        seance_id = self.request.data['seance']
        if Seance.objects.get(pk=seance_id).feuille.classe.enseignant != self.request.user:
            raise PermissionDenied("Vous n'êtes pas l'enseignant.")
        serializer.save()

    def perform_destroy(self, instance):
        if instance.seance.feuille.classe.enseignant != self.request.user:
            raise PermissionDenied("Vous n'êtes pas l'enseignant.")
        if instance.score is not None:
            raise PermissionDenied("Cette question a déjà été évaluée.")
        instance.delete()

    def perform_update(self, serializer):
        data = self.request.data
        # On ne peut ajouter des questions que pour des classes où l'on est enseignant
        if 'seance' in data \
                and Seance.objects.get(pk=data['seance']).feuille.classe.enseignant != self.request.user:
            raise PermissionDenied("Vous n'êtes pas l'enseignant")
        instance = self.get_object()
        # On ne peut pas modifier une question évaluée
        if instance.score is not None and \
                (('seance' in data and data['seance'] != instance.seance.id)
                 or ('position' in data and data['position'] != instance.seance.id)
                 or ('question' in data and data['question'] != instance.question.id)):
            raise PermissionDenied("On ne peut pas modifier une question évaluée.")
        serializer.save()


class ScoreListView(ListAPIView):
    queryset = Question.objects.all()

    def list(self, request, **kwargs):
        output_list = {}
        questions = super().get_queryset().filter(themes__id__contains=request.query_params.get('theme_id')) \
            .filter(questionsPosees__seance__feuille__classe__id=request.query_params.get('classe_id')).all()
        for question in questions:
            output_list[question.id] = QuestionPosee.objects.filter(
                question=question,
                seance__feuille__classe__id=request.query_params.get('classe_id')).values('score', 'seance__date')
        return Response(output_list)


class SeanceViewSet(ModelViewSet):
    serializer_class = serializers.SeanceSerializer
    permission_classes = [permissions.IsAuthenticated]
    queryset = Seance.objects.all()

    def get_queryset(self):
        return self.queryset.filter(feuille__classe__enseignant=self.request.user)

    def get_object(self):
        if self.request.method == 'PUT':
            seance = Seance.objects.filter(id=self.kwargs.get('pk')).first()
            if seance:
                return seance
            else:
                return Seance(id=self.kwargs.get('pk'))
        else:
            return super().get_object()

    @action(detail=True, methods=['get'])
    def get_pdf(self, request, pk):
        latexModel = LatexModel.objects.get(pk=request.query_params['model_id'])
        seance = self.get_object()
        result = latexModel.compile(seance=seance)
        if not result['status']:
            return HttpResponse(result['latexlog'].decode('utf-8').replace('\n', '<br>'))
        else:
            path = 'static/out/' + result['outfile']
            return FileResponse(open(path, 'rb'), as_attachment=True, content_type='application/pdf',
                                filename=latexModel.get_file_name(seance=seance) + '.pdf')

    def perform_create(self, serializer):
        seance = serializer.save()
        for seanceTmp in Seance.objects.filter(feuille=seance.feuille, position__gte=seance.position).exclude(
                id=seance.id).all():
            seanceTmp.position += 1
            seanceTmp.save()

    def perform_update(self, serializer):
        seance = serializer.save()
        for seanceTmp in Seance.objects.filter(feuille=seance.feuille, position__gte=seance.position).exclude(
                id=seance.id).all():
            seanceTmp.position += 1
            seanceTmp.save()

    def destroy(self, request, *args, **kwargs):
        seance = self.get_object()
        if QuestionPosee.objects.filter(seance=seance, score__isnull=False).count():
            raise PermissionDenied("Une séance évaluée ne peut pas être supprimée.")
        for seanceTmp in Seance.objects.filter(feuille=seance.feuille, position__gt=seance.position).all():
            seanceTmp.position += -1
            seanceTmp.save()
        return super().destroy(request, *args, **kwargs)


class SeanceViewSetR(ModelViewSet):
    serializer_class = SeanceSerializerR
    permission_classes = [permissions.IsAuthenticated]
    queryset = Seance.objects.all()

    def get_queryset(self):
        queryset = Seance.objects.filter(feuille__classe__enseignant=self.request.user)
        params = self.request.query_params
        if 'classe_id' in params:
            queryset = queryset.filter(feuille__classe__id=params['classe_id'])
        if 'feuille_id' in params:
            queryset = queryset.filter(feuille__id=params['feuille_id'])
        return queryset

class ThemeViewSet(ModelViewSet):
    serializer_class = ThemeSerializer
    queryset = Theme.objects.all()
    permission_classes = [permissions.IsAuthenticated]

    @action(methods=['get'], detail=True)
    def question_list(self, request, pk):
        questions = Question.objects.filter(themes__id=pk).order_by('questionorder__position')
        return Response(questions.values_list('id', flat=True))\

    @action(methods=['get'], detail=True)
    def get_evolution(self, request, pk):
        queue = [pk]
        themeIds = []
        while len(queue):
            theme = Theme.objects.get(pk=queue.pop())
            if len(theme.enfants.all()):
                for enfant in theme.enfants.all():
                    queue.append(enfant.id)
            else:
                themeIds.append(theme.id)
        qpIds = []
        for feuille in Classe.objects.get(pk=request.query_params['classe_id']).feuilles.all():
            for seance in feuille.seances.all():
                for qp in seance.questionsPosees.filter(question__themes__id__in=themeIds):
                    qpIds.append(qp.id)
        return Response(
            QuestionPoseeSerializer(QuestionPosee.objects.filter(id__in=qpIds)
                                    .order_by('seance__feuille__position', 'seance__position', 'position'),
            many=True).data)

    @action(detail=True, methods=['get'])
    def get_next_position(self, request, pk):
        theme = Theme.objects.get(pk=pk)
        newPosition = theme.get_next_position(
            float(request.query_params.get('position')),
            'before' in request.query_params)
        return Response(newPosition)
