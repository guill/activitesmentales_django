# Generated by Django 3.1.5 on 2021-01-21 20:28

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('am', '0002_classe_feuille_lienexosseances_lienexosthemes_question_seance_theme'),
    ]

    operations = [
        migrations.AlterField(
            model_name='classe',
            name='anneedebut',
            field=models.IntegerField(verbose_name=django.core.validators.MinValueValidator(2020)),
        ),
    ]
