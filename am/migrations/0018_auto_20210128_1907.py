# Generated by Django 3.1.5 on 2021-01-28 18:07

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('am', '0017_auto_20210127_2201'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='questionposee',
            constraint=models.UniqueConstraint(fields=('seance', 'position'), name='unique_question_position'),
        ),
    ]
