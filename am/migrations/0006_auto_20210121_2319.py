# Generated by Django 3.1.5 on 2021-01-21 22:19

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('am', '0005_auto_20210121_2213'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='classe',
            name='anneefin',
        ),
        migrations.AlterField(
            model_name='classe',
            name='anneedebut',
            field=models.IntegerField(verbose_name="Année de début de l'année scolaire"),
        ),
    ]
