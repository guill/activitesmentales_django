# Generated by Django 4.0.4 on 2022-05-03 15:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('am', '0038_auto_20220503_1714'),
    ]

    operations = [
        migrations.AlterField(
            model_name='utilisateur',
            name='nom_latex',
            field=models.CharField(max_length=100, verbose_name='nom LaTeX'),
        ),
    ]
