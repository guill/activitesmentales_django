# Generated by Django 3.1.5 on 2021-01-25 15:35

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('am', '0013_auto_20210125_1633'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='validee',
            field=models.BooleanField(blank=True, default=False, null=True),
        ),
    ]
