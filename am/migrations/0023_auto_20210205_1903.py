# Generated by Django 3.1.5 on 2021-02-05 18:03

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('am', '0022_auto_20210205_1855'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='classe',
            unique_together=set(),
        ),
        migrations.AddConstraint(
            model_name='classe',
            constraint=models.UniqueConstraint(fields=('enseignant', 'annee', 'nom'), name='unique_nom_classe'),
        ),
    ]
