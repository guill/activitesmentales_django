from django.contrib.auth import views as auth_views
from django.urls import include, path, reverse_lazy
from django.views.generic import TemplateView
from rest_framework.routers import SimpleRouter

from am import views

app_name = 'am'

router = SimpleRouter()
router.register(r'classe', views.ClasseViewSet, 'classe')
# router.register(r'feuiller', views.FeuilleViewSetR, 'feuiller')
router.register(r'feuille', views.FeuilleViewSet, 'feuille')
router.register(r'latexmodel', views.LatexModelViewSet)
router.register(r'latexmodelpart', views.LatexModelPartViewSet)
router.register(r'question', views.QuestionViewSet, 'question')
router.register(r'question-posee', views.QuestionPoseeViewSet, 'question-posee')
router.register(r'seance', views.SeanceViewSet, 'seance')
router.register(r'seancer', views.SeanceViewSetR, 'seancer')
router.register(r'theme', views.ThemeViewSet, 'theme')

urlpatterns = [
    path('', TemplateView.as_view(template_name='index.html'), name='home'),
    path('password_reset/', auth_views.PasswordResetView.as_view(
        success_url=reverse_lazy('am:password_reset_done'),
    ),
         name='password_reset'
         ),
    path('reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(success_url=reverse_lazy('am:password_reset_complete')),
         name='password_reset_confirm'
         ),
    path('', include('django.contrib.auth.urls')),
    path('signup/', views.SignUpView.as_view(), name='signup'),
    path('profile/', views.UserChangeView.as_view(), name='profile'),
    path('password/', auth_views.PasswordChangeView.as_view(
        template_name='registration/password_change.html',
        success_url='/am/profile/'
    ), name='change_password'),
    path('api/', include((router.urls, app_name))),
    path('api/annee_list/', views.AnneeView.as_view()),
]
