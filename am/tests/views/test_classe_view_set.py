from django.test import TestCase

from am.models import Utilisateur, Classe


class ClasseViewSetTestCase(TestCase):
    def setUp(self):
        self.user1 = Utilisateur.objects.create_user(username='user1', password='secret1')
        self.user2 = Utilisateur.objects.create_user(username='user2', password='secret2')
        self.classe_u1 = Classe.objects.create(nom='classe u1', nom_latex='classe', annee=2020, enseignant=self.user1)
        self.classe_u2 = Classe.objects.create(nom='classe u2', nom_latex='classe', annee=2020, enseignant=self.user2)

    def test_get(self):
        '''
        Tests  the download of one or many Classe
        '''
        reponse = self.client.get('/am/api/classe/')
        self.assertIn(reponse.status_code, {401, 403}, reponse)

        # Détails d'une classe, utilisateur non identifié
        reponse = self.client.get('/am/api/classe/{}/'.format(self.classe_u1.id))
        self.assertEqual(reponse.status_code, 403, str(reponse.json()))

        # Utilisateur identifié : Question dont il est l'enseignant ou non
        self.client.login(username='user1', password='secret1')
        reponse = self.client.get('/am/api/classe/')
        self.assertEqual(reponse.status_code, 200, str(reponse.json()))
        liste = reponse.json()
        self.assertEqual(len(list(filter(lambda classe: classe['id'] == self.classe_u1.id, liste))), 1,
                         str(reponse.json()))
        self.assertEqual(len(list(filter(lambda classe: classe['id'] == self.classe_u2.id, liste))), 0,
                         str(reponse.json()))

        # Détails d'une classe dont il est l'enseignant ou non
        reponse = self.client.get('/am/api/classe/{}/'.format(self.classe_u1.id))
        self.assertEqual(reponse.status_code, 200, str(reponse.json()))

        reponse = self.client.get('/am/api/classe/{}/'.format(self.classe_u2.id))
        self.assertEqual(reponse.status_code, 404, str(reponse.json()))

    def test_post(self):
        '''
        Test the creation of a Classe
        :return:
        '''
        # Utilisateur non identifié
        reponse = self.client.post('/am/api/classe/',
                                   data={'nom': 'classe1', 'nom_latex': 'classe1', 'annee': 2020},
                                   content_type='application/json')
        self.assertIn(reponse.status_code, {401, 403}, reponse)

        # Utilisateur identifié
        self.client.login(username='user1', password='secret1')
        reponse = self.client.post('/am/api/classe/',
                                   data={'nom': 'classe1', 'nom_latex': 'classe1', 'annee': 2020},
                                   content_type='application/json')
        self.assertEqual(reponse.status_code, 201, str(reponse.json()))

        # sans annee
        reponse = self.client.post('/am/api/classe/',
                                   data={'nom': 'classe1', 'nom_latex': 'classe1'},
                                   content_type='application/json')
        self.assertEqual(reponse.status_code, 400, str(reponse.json()))

        # annee non numérique
        reponse = self.client.post('/am/api/classe/',
                                   data={'nom': 'classe1', 'nom_latex': 'classe1', 'annee': 'deux-mille'},
                                   content_type='application/json')
        self.assertEqual(reponse.status_code, 400, str(reponse.json()))

        # sans nom_latex
        reponse = self.client.post('/am/api/classe/',
                                   data={'nom': 'classe1', 'nom_latex': '', 'annee': 2020},
                                   content_type='application/json')
        self.assertEqual(reponse.status_code, 400, str(reponse.json()))

        # sans nom
        reponse = self.client.post('/am/api/classe/',
                                   data={'nom': '', 'nom_latex': 'classe1', 'annee': 'deux-mille'},
                                   content_type='application/json')
        self.assertEqual(reponse.status_code, 400, str(reponse.json()))

    def test_delete(self):
        """
        Test the deletion of a Classe
        """

        # Utilisateur non identifié
        reponse = self.client.delete('/am/api/classe/{}/'.format(self.classe_u1.id))
        # self.assertGreaterEqual(reponse.status_code, 400, str(reponse.json()))
        self.assertGreaterEqual(reponse.status_code, 400, reponse)
        self.assertEqual(Classe.objects.filter(pk=self.classe_u1.id).count(), 1)

        # Utilisateur identifié
        self.client.login(username='user1', password='secret1')
        reponse = self.client.delete('/am/api/classe/{}/'.format(self.classe_u1.id))
        self.assertLess(reponse.status_code, 300)
        self.assertEqual(Classe.objects.filter(pk=self.classe_u1.id).count(), 0)

        # Question dont l'utilisateur n'est pas l'enseignant
        reponse = self.client.delete('/am/api/classe/{}/'.format(self.classe_u2.id))
        self.assertGreaterEqual(reponse.status_code, 400, str(reponse.json()))
        self.assertEqual(Classe.objects.filter(pk=self.classe_u2.id).count(), 1)

    def test_put(self):
        '''
        Test the update of a Classe
        '''

        # Utilisateur non identifié
        reponse = self.client.put('/am/api/classe/{}/'.format(self.classe_u2.id),
                                  data={'nom': 'nouveau nom', 'nom_latex': 'nouveau', 'annee': 2021},
                                  content_type='application/json')
        self.assertGreaterEqual(reponse.status_code, 400, reponse)

        # Utilisateur identifié
        self.client.login(username='user1', password='secret1')
        reponse = self.client.put('/am/api/classe/{}/'.format(self.classe_u1.id),
                                  data={'nom': 'nouveau nom', 'nom_latex': 'nouveau', 'annee': 2021},
                                  content_type='application/json')
        self.assertLess(reponse.status_code, 300)
        self.assertEqual(Classe.objects.get(pk=self.classe_u1.id).nom,
                         'nouveau nom')

        # L'utilisateur n'est pas l'enseignant de la  nouvelle séance
        reponse = self.client.put('/am/api/classe/{}/'.format(self.classe_u2.id),
                                  data={'nom': 'nouveau nom', 'nom_latex': 'nouveau', 'annee': 2021},
                                  content_type='application/json')
        self.assertGreaterEqual(reponse.status_code, 400, str(reponse.json()))
        self.assertNotEqual(Classe.objects.get(pk=self.classe_u2.id).nom,
                            'nouveau nom')
