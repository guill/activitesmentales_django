from django.db.utils import IntegrityError
from django.test import TestCase

from am.models import Utilisateur, Seance, QuestionPosee


class QuestionPoseeViewSetTestCase(TestCase):
    fixtures = ['objects.json']

    def setUp(self):
        self.user1 = Utilisateur.objects.get(pk=3)
        self.user2 = Utilisateur.objects.get(pk=4)
        self.validateur = Utilisateur.objects.get(pk=2)
        # self.question11 = Question.objects.get(pk=11) # not avalider
        # self.question12 = Question.objects.get(pk=12) # avalider
        self.questionPosee1 = QuestionPosee.objects.get(pk=1)
        # self.question14 = Question.objects.get(pk=14) # validee
        self.seance11 = Seance.objects.get(pk=11)
        # self.seance20 = Seance.objects.get(pk=20)
        self.client.force_login(self.user1)

    def test_getall_unlogged(self):
        self.client.logout()
        response = self.client.get('/am/api/question-posee/')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_getone_unlogged(self):
        self.client.logout()
        response = self.client.get('/am/api/question-posee/1/')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_getall(self):
        response = self.client.get('/am/api/question-posee/')
        self.assertEqual(response.status_code, 200, response)
        questionPosee_ids = [item['id'] for item in response.json()]
        self.assertIn(1, questionPosee_ids)

    def test_getone(self):
        response = self.client.get('/am/api/question-posee/1/')
        self.assertEqual(response.status_code, 200, response)
        self.assertEqual(response.json()['id'], 1)

    def test_getone_not_author(self):
        self.client.force_login(self.user2)
        response = self.client.get('/am/api/question-posee/1/')
        self.assertGreaterEqual(response.status_code, 400, response)

    # TODO : to continue
    # test with parameters

    def test_post_unlogged(self):
        self.client.logout()
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 10, 'position': 3, 'question': 14},
                                    content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_post(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 10, 'position': 3, 'question': 14},
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201, response)

    def test_post_no_seance(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'position': 3, 'question': 14},
                                    content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_post_unknow_seance(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': -1, 'position': 3, 'question': 14},
                                    content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_post_not_teacher(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 20, 'position': 3, 'question': 14},
                                    content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_post_no_position(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 10, 'question': 14},
                                    content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_post_position_less_than_1(self):
        with self.assertRaises(IntegrityError, msg='La position doit être comprise entre 1 et 5'):
            self.client.post('/am/api/question-posee/',
                             data={'seance': 10, 'position': 0, 'question': 14},
                             content_type='application/json')

    def test_post_position_greater_than_5(self):
        with self.assertRaises(IntegrityError, msg='La position doit être comprise entre 1 et 5'):
            self.client.post('/am/api/question-posee/',
                             data={'seance': 10, 'position': 6, 'question': 14},
                             content_type='application/json')

    def test_post_used_position(self):
        with self.assertRaises(IntegrityError, msg='2 Questions posées à la même position'):
            self.client.post('/am/api/question-posee/',
                             data={'seance': 10, 'position': 5, 'question': 14},
                             content_type='application/json')

    def test_post_no_question(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 20, 'position': 3},
                                    content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_post_unknow_question(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 20, 'position': 3, 'question': -1},
                                    content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_post_not_avalider(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 10, 'position': 3, 'question': 11},
                                    content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_post_avalider(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 10, 'position': 3, 'question': 12},
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201, response)

    def test_post_avalider_not_author(self):
        self.client.force_login(self.user2)
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 20, 'position': 3, 'question': 12},
                                    content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_post_validee_not_author(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 10, 'position': 3, 'question': 22},
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201, response)

    def test_post_with_score(self):
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 10, 'position': 3, 'question': 14, 'score': 0.7},
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201, response)

    def test_post_with_score_null(self):
        self.questionPosee1.score = .7
        self.questionPosee1.save()
        response = self.client.post('/am/api/question-posee/',
                                    data={'seance': 10, 'position': 3, 'question': 14, 'score': None},
                                    content_type='application/json')
        self.assertEqual(response.status_code, 201, response)

    def test_post_with_score_less_than_0(self):
        with self.assertRaises(IntegrityError, msg='Le score doit être compris entre 0 et 1'):
            self.client.post('/am/api/question-posee/',
                             data={'seance': 10, 'position': 3, 'question': 14, 'score': -0.7},
                             content_type='application/json')

    def test_post_with_score_greater_than_1(self):
        with self.assertRaises(IntegrityError, msg='Le score doit être compris entre 0 et 1'):
            self.client.post('/am/api/question-posee/',
                             data={'seance': 10, 'position': 3, 'question': 14, 'score': 1.5},
                             content_type='application/json')

    #
    # Delete
    #
    def test_delete_unlogged(self):
        self.client.logout()
        response = self.client.delete('/am/api/question-posee/1/')
        self.assertGreaterEqual(response.status_code, 400)

    def test_delete(self):
        response = self.client.delete('/am/api/question-posee/1/')
        self.assertLess(response.status_code, 300)
        self.assertEqual(QuestionPosee.objects.filter(pk=1).count(), 0)

    def test_delete_not_teacher(self):
        self.client.force_login(self.user2)
        response = self.client.delete('/am/api/question-posee/1/')
        self.assertGreaterEqual(response.status_code, 400, response)
        self.assertEqual(QuestionPosee.objects.filter(pk=1).count(), 1)

    def test_delete_unknown(self):
        response = self.client.delete('/am/api/question-posee/-1/')
        self.assertGreaterEqual(response.status_code, 400)

    def test_delete_rated(self):
        self.questionPosee1.score = 0.5
        self.questionPosee1.save()
        response = self.client.delete('/am/api/question-posee/1/')
        self.assertGreaterEqual(response.status_code, 400, response)
        self.assertEqual(QuestionPosee.objects.filter(pk=1).count(), 1)

    def test_delete_rated_0(self):
        self.questionPosee1.score = 0
        self.questionPosee1.save()
        response = self.client.delete('/am/api/question-posee/1/')
        self.assertGreaterEqual(response.status_code, 400, response)
        self.assertEqual(QuestionPosee.objects.filter(pk=1).count(), 1)

    #
    # Updates
    #
    def test_patch_unlogged(self):
        self.client.logout()
        response = self.client.patch('/am/api/question-posee/1/', data={'seance': 10, 'position': 4, 'question': 14},
                                     content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_seance(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'seance': 12}, content_type='application/json')
        self.assertLess(response.status_code, 300, response)
        self.assertEqual(QuestionPosee.objects.get(pk=1).seance.id, 12)

    def test_patch_no_seance(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'seance': None}, content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_unknown_seance(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'seance': -1}, content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_seance_not_teacher(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'seance': 20}, content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_seance_question_rated(self):
        self.questionPosee1.score = 0.8
        self.questionPosee1.save()
        response = self.client.patch('/am/api/question-posee/1/', data={'seance': 12}, content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_seance_used_slot(self):
        self.questionPosee1.position = 5
        self.questionPosee1.save()
        with self.assertRaises(IntegrityError, msg='Cette position est occupée.'):
            self.client.patch('/am/api/question-posee/1/', data={'seance': 11}, content_type='application/json')

    def test_patch_position(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'position': 4}, content_type='application/json')
        self.assertEqual(response.status_code, 200, response)

    def test_patch_no_position(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'position': None}, content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_used_position(self):
        self.questionPosee1.seance = self.seance11
        self.questionPosee1.position = 4
        self.questionPosee1.save()
        with self.assertRaises(IntegrityError, msg='Cette position est occupée.'):
            self.client.patch('/am/api/question-posee/1/', data={'position': 5}, content_type='application/json')

    def test_patch_position_less_than_1(self):
        with self.assertRaises(IntegrityError, msg='La position doit être comprise entre 1 et 5'):
            self.client.patch('/am/api/question-posee/1/', data={'position': 0},
                              content_type='application/json')

    def test_patch_position_greater_than_5(self):
        with self.assertRaises(IntegrityError, msg='La position doit être comprise entre 1 et 5'):
            self.client.patch('/am/api/question-posee/1/', data={'position': 6},
                              content_type='application/json')

    def test_patch_no_question(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'question': None}, content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_unknow_question(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'question': -1}, content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_not_avalider(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'question': 11}, content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_avalider(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'question': 12}, content_type='application/json')
        self.assertEqual(response.status_code, 200, response)

    def test_patch_avalider_not_author(self):
        self.client.force_login(self.user2)
        response = self.client.patch('/am/api/question-posee/1/', data={'question': 12}, content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, response)

    def test_patch_validee_not_author(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'question': 22}, content_type='application/json')
        self.assertEqual(response.status_code, 200, response)

    def test_patch_score(self):
        response = self.client.patch('/am/api/question-posee/1/', data={'score': 0.7}, content_type='application/json')
        self.assertEqual(response.status_code, 200, response)

    def test_patch_score_null(self):
        self.questionPosee1.score = .7
        self.questionPosee1.save()
        response = self.client.patch('/am/api/question-posee/1/', data={'score': None}, content_type='application/json')
        self.assertEqual(response.status_code, 200, response)

    def test_post_score_less_than_0(self):
        with self.assertRaises(IntegrityError, msg='Le score doit être compris entre 0 et 1'):
            self.client.patch('/am/api/question-posee/1/', data={'score': -0.7}, content_type='application/json')

    def test_patch_score_greater_than_1(self):
        with self.assertRaises(IntegrityError, msg='Le score doit être compris entre 0 et 1'):
            self.client.patch('/am/api/question-posee/1/', data={'score': 1.5}, content_type='application/json')
