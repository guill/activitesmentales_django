import os

from django.test import TestCase

from am.models import Utilisateur, Question, Theme, Seance, QuestionPosee, QuestionOrder


class QuestionViewSetTestCase(TestCase):
    fixtures = ['objects.json']

    def setUp(self):
        self.user1 = Utilisateur.objects.get(pk=3)
        self.user2 = Utilisateur.objects.get(pk=4)
        self.validateur = Utilisateur.objects.get(pk=2)
        self.seance11 = Seance.objects.get(pk=11)
        self.seance12 = Seance.objects.get(pk=12)
        self.seance20 = Seance.objects.get(pk=20)
        self.theme1 = Theme.objects.get(pk=1)
        self.soustheme21 = Theme.objects.get(pk=21)
        self.soustheme22 = Theme.objects.get(pk=22)
        self.soustheme31 = Theme.objects.get(pk=31)
        self.soustheme32 = Theme.objects.get(pk=32)
        self.question1 = Question.objects.create(code="\\Latex", thumb_name='thumb', auteur=self.user1)
        self.question2 = Question.objects.create(code="\\Latex{}2", thumb_name='thumb', auteur=self.user2)
        self.client.force_login(self.user1)

    #
    # Get
    #

    def test_get_all_unlogged(self):
        self.client.logout()
        response = self.client.get('/am/api/question/')
        self.assertIn(response.status_code, {401, 403}, "Unlogged user:\n" + str(response.json()))

    def test_get_one_unlogged(self):
        self.client.logout()
        response = self.client.get('/am/api/question/{}/'.format(self.question1.id))
        self.assertIn(response.status_code, {401, 403}, "Unlogged user :\n" + str(response.json()))

    def test_get_all(self):
        response = self.client.get('/am/api/question/')
        self.assertEqual(response.status_code, 200, "Get all the questions:\n" + str(response.json()))
        questions_ids = [item['id'] for item in response.json()]
        self.assertIn(self.question1.id, questions_ids)
        self.assertNotIn(self.question2.id, questions_ids)

    def test_get_one_owned(self):
        response = self.client.get('/am/api/question/{}/'.format(self.question1.id))
        self.assertEqual(response.status_code, 200, "Get user's question:\n" + str(response.json()))

    def test_get_one_not_owned(self):
        response = self.client.get('/am/api/question/{}/'.format(self.question2.id))
        self.assertEqual(response.status_code, 404, "Get not user's question :\n" + str(response.json()))

    def test_get_all_not_owned_avalider(self):
        self.question2.avalider = True
        self.question2.save()
        response = self.client.get('/am/api/question/')
        self.assertNotIn(self.question2.id, [item['id'] for item in response.json()])

    def test_get_one_not_owned_avalider(self):
        self.question2.avalider = True
        self.question2.save()
        response = self.client.get('/am/api/question/{}/'.format(self.question2.id))
        self.assertEqual(response.status_code, 404, "Get not user's question, avalidee :\n" + str(response.json()))

    def get_all_not_owned_used(self):
        QuestionPosee.objects.create(seance=self.seance20, question=self.question2, position=1)
        response = self.client.get('/am/api/question/')
        self.assertNotIn(self.question2.id, [item['id'] for item in response.json()])

    def get_one_not_owned_used(self):
        QuestionPosee.objects.create(seance=self.seance20, question=self.question2, position=1)
        response = self.client.get('/am/api/question/{}/'.format(self.question2.id))
        self.assertEqual(response.status_code, 404, "Get not user's question, used :\n" + str(response.json()))

    def get_all_not_owned_validee(self):
        self.question2.validee = True
        self.question2.save()
        response = self.client.get('/am/api/question/')
        self.assertIn(self.question2.id, [item['id'] for item in response.json()])

    def get_one_not_owned_validee(self):
        self.question2.validee = True
        self.question2.save()
        response = self.client.get('/am/api/question/{}/'.format(self.question2.id))
        self.assertEqual(response.status_code, 200, "Get not user's question, validee :\n" + str(response.json()))

    #
    # Creation
    #
    def test_create_unlogged(self):
        self.client.logout()
        question = {
            'code': '$3\\times4$',
            'themes': []
        }
        question['themes'].append(self.soustheme21.id)
        response = self.client.post('/am/api/question/', question)
        self.assertIn(response.status_code, {401, 403}, "Unlogged user")

    def test_create(self):
        question = {
            'code': '$3\\times 4$',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme21.id, "position": 5})
        response = self.client.post('/am/api/question/', question, content_type='application/json')
        if 'latexlog' in response.json():
            print(response.json()['latexlog'])
        self.assertEqual(Question.objects.get(pk=response.data['id']).auteur, self.user1)
        self.assertTrue(os.path.isfile('static/thumbs/' + response.data['thumb_name']))

    def test_create_avalider(self):
        question = {
            'code': '$3\\times 4$',
            'themes': [],
            'avalider': True,
        }
        question['themes'].append({"theme": self.soustheme21.id, "position": 5})
        response = self.client.post('/am/api/question/', question, content_type='application/json')
        self.assertEqual(response.status_code, 201, str(response.json()))

    def test_create_validee(self):
        question = {
            'code': '$3\\times 4$',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme21.id, "position": 5})
        response = self.client.post('/am/api/question/', question, content_type='application/json')
        self.assertEqual(Question.objects.get(pk=response.json()['id']).auteur, self.user1)

    def test_create_without_code(self):
        question = {
            'code': '',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme21.id, "position": 5})
        response = self.client.post('/am/api/question/', question, content_type='application/json')
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    def test_create_with_code_err(self):
        question = {
            'code': '$2\\times 6',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme21.id, "position": 0})
        response = self.client.post('/am/api/question/', question, content_type='application/json')
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    def test_create_without_theme(self):
        question = {
            'code': '$3\\times 5$',
            'themes': []
        }
        response = self.client.post('/am/api/question/', question, content_type='application/json')
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    def test_create_on_not_leaf_theme(self):
        question = {
            'code': '$3\\times4$',
            'themes': []
        }
        question['themes'].append({"theme": self.theme1.id, "position": 5})
        response = self.client.post('/am/api/question/', question, content_type='application/json')
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    #
    # Update
    #
    def test_patch(self):
        question = {
            'code': '$6\\times 2$',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme32.id, "position": None})
        question['themes'].append({"theme": self.soustheme22.id, "position": 5.2})
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     data=question,
                                     content_type='application/json')
        self.assertEqual(Question.objects.get(pk=self.question1.id).code, question['code'])
        # self.assertEqual(list(Question.objects.get(pk=self.question1.id).themes.values_list('id', flat=True)).sort(),
        #                  question['themes'].sort())
        self.assertTrue(os.path.isfile('static/thumbs/' + response.data['thumb_name']))

    def test_patch_not_owned(self):
        question = {
            'code': '$6\\times 2$',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme32.id, "position": None})
        question['themes'].append({"theme": self.soustheme22.id, "position": 5})
        response = self.client.patch('/am/api/question/{}/'.format(self.question2.id),
                                     data=question,
                                     content_type='application/json')
        self.assertEqual(response.status_code, 404)

    def test_patch_question_avalider(self):
        self.question1.avalider = True
        self.question1.save()
        question = {
            'code': '$6\\times 2$',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme32.id, "position": None})
        question['themes'].append({"theme": self.soustheme22.id, "position": 5})
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     data=question,
                                     content_type='application/json')
        self.assertEqual(Question.objects.get(pk=self.question1.id).code, question['code'])
        self.assertEqual(Question.objects.get(pk=self.question1.id).themes.count(), len(question['themes']))
        for qo in question['themes']:
            if qo['position'] is not None:
                self.assertEqual(
                    Question.objects.get(pk=self.question1.id).questionorder_set.get(theme=qo['theme']).position,
                    qo['position'])

    def test_patch_avalider_unordered(self):
        self.question1.themes.set([self.soustheme21])
        self.question1.save()
        question = {
            'avalider': True
        }
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     data=question,
                                     content_type='application/json')
        self.assertEqual(response.status_code, 200, str(response.json()))

    def test_patch_avalider_with_new_theme(self):
        question = {
            'avalider': True,
            'themes': [{"theme": 32, "position": None}]
        }
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     data=question,
                                     content_type='application/json')
        self.assertEqual(response.status_code, 200, str(response.json()))

    def test_patch_avalider_ordered(self):
        self.question1.themes.set([self.soustheme21],  through_defaults={"position": 25.0})
        self.question1.save()
        qo = QuestionOrder.objects.get(question=self.question1, theme=self.soustheme21)
        qo.position = 0
        qo.save()
        question = {
            'avalider': True
        }
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     data=question,
                                     content_type='application/json')
        self.assertEqual(response.status_code, 200)

    def test_patch_question_used(self):
        self.question1.avalider = True
        self.question1.save()
        QuestionPosee.objects.create(seance=self.seance12, position=1, question=self.question1)
        question = {
            'code': '$6\\times2$',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme22.id, "position": 0.0})
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     data=question,
                                     content_type='application/json')
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    def test_patch_question_validee(self):
        self.question1.avalider = True
        self.question1.validee = True
        self.question1.save()
        question = {
            'code': '$6\\times2$',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme22.id, "position": 0.0})
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     data=question,
                                     content_type='application/json')
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    def test_patch_validateur(self):
        self.client.force_login(self.validateur)
        self.question1.avalider = True
        self.question1.validee = True
        self.question1.save()
        question = {
            'code': '$6\\times2$',
            'themes': []
        }
        question['themes'].append({"theme": self.soustheme32.id, "position": 0.0})
        question['themes'].append({"theme": self.soustheme22.id, "position": -5})
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     data=question,
                                     content_type='application/json')
        self.assertEqual(Question.objects.get(pk=self.question1.id).code, question['code'])
        self.assertEqual(Question.objects.get(pk=self.question1.id).themes.count(), len(question['themes']))
        for qo in question['themes']:
            if qo['position'] is not None:
                self.assertEqual(
                    Question.objects.get(pk=self.question1.id).questionorder_set.get(theme=qo['theme']).position,
                    qo['position'])

    def test_patch_without_code(self):
        question = {
            'code': '',
        }
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     question,
                                     content_type='application/json')
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    def test_patch_with_code_err(self):
        question = {
            'code': '$2\\times 6'
        }
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     question,
                                     content_type='application/json')
        self.assertGreaterEqual(response.status_code, 400, str(response.json()))
        self.assertEqual(Question.objects.get(pk=self.question1.id).code, self.question1.code)

    def test_patch_without_theme(self):
        question = {
            'themes': []
        }
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     question,
                                     content_type='application/json')
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    def test_patch_with_not_leaf_theme(self):
        question = {
            'themes': []
        }
        question['themes'].append({"theme": self.theme1.id, "position": 0.0})
        response = self.client.patch('/am/api/question/{}/'.format(self.question1.id),
                                     question,
                                     content_type='application/json')
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    #
    # delete
    #
    def test_delete_unlogged(self):
        self.client.logout()
        response = self.client.delete('/am/api/question/{}/'.format(self.question1.id))
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    def test_delete(self):
        response = self.client.delete('/am/api/question/{}/'.format(self.question1.id))
        self.assertEqual(response.status_code, 204)

    def test_delete_not_owner(self):
        response = self.client.delete('/am/api/question/{}/'.format(self.question2.id))
        self.assertEqual(response.status_code, 404, str(response.json()))

    def test_delete_avalider(self):
        self.question1.avalider = True
        self.question1.save()
        response = self.client.delete('/am/api/question/{}/'.format(self.question1.id))
        self.assertEqual(response.status_code, 204)

    def test_delete_used(self):
        self.question1.avalider = True
        self.question1.save()
        QuestionPosee.objects.create(seance=self.seance12, position=1, question=self.question1)
        response = self.client.delete('/am/api/question/{}/'.format(self.question1.id))
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))

    def test_delete_validee(self):
        self.question1.avalider = True
        self.question1.validee = True
        self.question1.save()
        response = self.client.delete('/am/api/question/{}/'.format(self.question1.id))
        self.assertIn(response.status_code, {400, 401, 403}, str(response.json()))
