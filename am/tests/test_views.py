from django.test import TestCase, Client

from am.models import Utilisateur, Classe


class ClasseEditTestCase(TestCase):
    def setUp(self):
        self.user1 = Utilisateur.objects.create_user(username='jacob', password='top_secret')
        self.user2 = Utilisateur.objects.create_user(username='machin', password='top_secret2')
        self.classe1 = Classe.objects.create(nom='classe1', nom_latex='idem', annee=2020, enseignant=self.user1)
        self.classe2 = Classe.objects.create(nom='classe2', nom_latex='idem', annee=2019, enseignant=self.user2)
        self.client = Client()

    # def test_classe_creation(self):
    #     """
    #     Teste les créations de classes
    #     """
    #     reponse = self.client.get('/am/classe/')
    #     self.assertEqual(reponse.status_code, 403,      # 401,?
    #                      "les utilisateurs non identifiés n'ont pas le droit de créer une classe")
    #     # Connexion
    #     self.client.login(username='jacob', password='top_secret')
    #     response = self.client.post('/am/classe/',
    #                                 data={'nom': 'maClasse1', 'nom_latex': 'maClasse', 'annee': 2020})
    #     self.assertEqual(response.status_code, 302,
    #                      "Un utilisateur identifié peut créer une classe")
    #     self.classe1 = Classe.objects.latest('id')
    #     self.assertEqual(self.classe1.enseignant.username, 'jacob',
    #                      "la classe créée prend pour enseignant l'utilisateur qui la crée")

    def test_classe_edition(self):
        """
        Teste la modification de classe
        """
        response = self.client.get("/am/api/classe/%d/" % self.classe1.id)
        # print('response', response)
        self.assertIn(response.status_code, {401, 403},
                         "Un utilisateur non identifié ne peut pas modifier de classe.")
        # Connexion
        self.assertTrue(self.client.login(username='jacob', password='top_secret'), 'Erreur de connexion')
        reponse = self.client.get("/am/api/classe/%d/" % self.classe1.id)
        self.assertEqual(reponse.status_code, 200,
                         "Un utilisateur peut modifier les classes dont il est l'enseignant.")
        reponse = self.client.get("/am/api/classe/%d/" % self.classe2.id)
        self.assertEqual(reponse.status_code, 404,  # 403,?
                         "Un utilisateur ne peut pas modifier les classes dont il n'est pas l'enseignant")
        reponse = self.client.get("/am/api/classe/%d/" % (self.classe2.id + 1))
        self.assertEqual(reponse.status_code, 404,
                         "Classe non existante")


class ClasseListTestCase(TestCase):
    def setUp(self):
        self.user1 = Utilisateur.objects.create_user(username='jacob', password='top_secret')
        self.user2 = Utilisateur.objects.create_user(username='machin', password='top_secret2')
        self.classe1 = Classe.objects.create(nom='classe1', nom_latex='idem', annee=2020, enseignant=self.user1)
        self.classe2 = Classe.objects.create(nom='classe2', nom_latex='idem', annee=2019, enseignant=self.user1)
        self.classe3 = Classe.objects.create(nom='classe3', nom_latex='idem', annee=2020, enseignant=self.user2)
        self.client = Client()

    def test_classe_visible(self):
        """
        Teste l'affichage des classes en fonction de l'utilisateur connecté
        """
        reponse = self.client.get('/am/api/classe/?annee=2020')
        self.assertIn(reponse.status_code, {401, 403},
                         "les utilisateurs non identifiés n'ont pas le droit d'afficher les classes.")
        # Connexion
        self.assertTrue(self.client.login(username='jacob', password='top_secret'), 'Erreur de connexion')

        # test pour 2020
        reponse = self.client.get('/am/api/classe/?annee=2020')
        self.assertEqual(reponse.status_code, 200)

        # TODO :
        # self.assertContains(reponse, 'classe1', html=True,
        #                     msg_prefix="Les utilisateurs voient les classes dont ils sont enseignant.")
        # self.assertNotContains(reponse, 'classe2', html=True,
        #                        msg_prefix="Seules les classes correspondant à l'année demandée sont affichées.")
        # self.assertNotContains(reponse, 'classe3', html=True,
        #                        msg_prefix="Les utilisateurs ne voient pas les classes dont ils ne sont pas enseignant.")

        # test pour 2019
        reponse = self.client.get('/am/api/classe/?annee=2019')
        self.assertEqual(reponse.status_code, 200)

        # TODO :
        # self.assertNotContains(reponse, 'classe1', html=True,
        #                        msg_prefix="Seules les classes correspondant à l'année demandée sont affichées.")
        # self.assertContains(reponse, 'classe2', html=True,
        #                     msg_prefix="Les utilisateurs voient les classes dont ils sont enseignant.")
        # self.assertNotContains(reponse, 'classe3', html=True,
        #                        msg_prefix="Les utilisateurs ne voient pas les classes dont ils ne sont pas enseignant.")

        # test pour user2
        self.assertTrue(self.client.login(username=self.user2.username, password='top_secret2'), 'Erreur de connexion')
        reponse = self.client.get('/am/api/classe/?annee=2020')
        self.assertEqual(reponse.status_code, 200)

        # TODO :
        # self.assertNotContains(reponse, 'classe1', html=True,
        #                        msg_prefix="Les utilisateurs ne voient pas les classes dont ils ne sont pas enseignant.")
        # self.assertContains(reponse, 'classe3', html=True,
        #                     msg_prefix="Les utilisateurs voient les classes dont ils sont enseignant.")
