from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.forms import ModelForm

from .models import Utilisateur, Classe


class UtilisateurCreationForm(UserCreationForm):
    class Meta:
        model = Utilisateur
        fields = ('username', 'email', 'first_name', 'last_name', 'nom_latex')


class UtilisateurChangeForm(UserChangeForm):
    password = None

    class Meta:
        model = Utilisateur
        fields = ('username', 'email', 'first_name', 'last_name', 'nom_latex')


class ClasseForm(ModelForm):
    class Meta:
        model = Classe
        fields = ('nom', 'nom_latex', 'annee')
