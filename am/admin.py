from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .forms import UtilisateurCreationForm, UtilisateurChangeForm
from .models import Classe, Question, Theme, Utilisateur, Feuille, Seance, QuestionPosee


class UtilisateurAdmin(UserAdmin):
    add_form = UtilisateurCreationForm
    form = UtilisateurChangeForm
    model = Utilisateur
    list_display = ['email', 'username', ]


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('id',)


admin.site.register(Utilisateur, UtilisateurAdmin)
admin.site.register(Classe)
admin.site.register(Feuille)
admin.site.register(Question, QuestionAdmin)
admin.site.register(QuestionPosee)
admin.site.register(Seance)
admin.site.register(Theme)
