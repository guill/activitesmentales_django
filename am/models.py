import os
import re
import shutil
import tempfile
import uuid
from subprocess import CalledProcessError, run

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import Q
from pdf2image import pdf2image


class Utilisateur(AbstractUser):
    nom_latex = models.CharField("nom LaTeX", max_length=100)

    def __str__(self):
        return self.username

class Classe(models.Model):
    objects = None
    nom = models.CharField(max_length=100)
    nom_latex = models.CharField("nom LaTeX", max_length=100)
    annee = models.IntegerField("Année de début de l'année scolaire")
    enseignant = models.ForeignKey(Utilisateur, on_delete=models.PROTECT)

    class Meta:
        ordering = ['nom']
        constraints = [
            models.UniqueConstraint(fields=['enseignant', 'annee', 'nom'], name='unique_nom_classe')
        ]

    def __str__(self):
        return self.nom


class Feuille(models.Model):
    objects = None
    classe = models.ForeignKey(Classe, on_delete=models.CASCADE, related_name='feuilles')
    position = models.IntegerField()

    class Meta:
        ordering = ['position']

    def __str__(self):
        return 'Feuille_' + self.classe.nom + '_' + str(self.position)

    def get_position(self):
        return Feuille.objects.filter(classe=self.classe, position__lte=self.position).count()


class Question(models.Model):
    id = None
    objects = None
    code = models.TextField()
    auteur = models.ForeignKey(Utilisateur, on_delete=models.RESTRICT, default=2)
    avalider = models.BooleanField(default=False)
    validee = models.BooleanField(default=False)
    themes = models.ManyToManyField('Theme', related_name='questions', through='QuestionOrder')
    thumb_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        permissions = [
            ("question_valider", "Peut valider une question : la rendre disponible pour tous."),
        ]

    def compile(self):
        newName = uuid.uuid4().hex + '.png'
        # ensure uniqueness of thumb_name
        while Question.objects.filter(thumb_name=newName).count() > 0:
            newName = uuid.uuid4().hex + '.png'
        latexModel = LatexModel.objects.get(type=LatexModelType.QUESTION)
        result = latexModel.compile(question=self, outfile=newName)
        return result


class QuestionOrder(models.Model):
    objects = None
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    theme = models.ForeignKey('Theme', on_delete=models.CASCADE)
    position = models.FloatField(default=0)

    class Meta:
        unique_together = ['question', 'theme']
        ordering = ['position']


class QuestionPosee(models.Model):
    objects = None
    seance = models.ForeignKey('Seance', related_name='questionsPosees', on_delete=models.CASCADE)
    position = models.IntegerField()  # position de la question dans la séance ; la première position est 1
    question = models.ForeignKey(Question, related_name='questionsPosees', on_delete=models.CASCADE)
    score = models.FloatField(blank=True, null=True)

    class Meta:
        ordering = ['position']
        constraints = [
            models.UniqueConstraint(
                fields=['seance', 'position'],
                name='unique_question_position'
            ),
            models.CheckConstraint(
                check=(Q(score__gte=0.0) & Q(score__lte=1.0)),
                name='check_score_range',
            ),
            models.CheckConstraint(
                check=(Q(position__gte=1) & Q(position__lte=5)),
                name='check_position_range',
            ),
        ]


class Seance(models.Model):
    objects = None
    feuille = models.ForeignKey(Feuille, related_name='seances', on_delete=models.CASCADE)
    date = models.DateField(blank=True, null=True)
    position = models.IntegerField()

    class Meta:
        ordering = ['position']

    def get_position(self):
        return Seance.objects.filter(feuille=self.feuille, position__lte=self.position).count()



class Theme(models.Model):
    objects = None
    questions = None
    enfants = None
    label = models.CharField(max_length=100)
    parent = models.ForeignKey('self',
                               on_delete=models.RESTRICT,
                               related_name='enfants',
                               blank=True,
                               null=True)
    position = models.IntegerField(blank=True, null=True)
    ids_list = []

    class Meta:
        ordering = ['position']

    def __str__(self):
        return self.label

    def get_path(self):
        path = [self]
        parent = self.parent
        while parent:
            path.append(parent)
            parent = parent.parent
        path.reverse()
        return path

    def get_next_position(self, position: float, before=False) -> float:
        """
        :param position:
        :param before: if True, a position before the given one is returned
        :return: the position between the given position and the position of the next item.
            position + 100 if there's no such item
        :rtype: float
        """
        # TODO: check if there' no pb with multiple theme for one question (which position is considered ?)
        if before:
            next_question = self.questions.filter(questionorder__position__lt=position)
        else:
            next_question = self.questions.filter(questionorder__position__gt=position)
        if len(next_question):
            if before:
                next_position = next_question.order_by('questionorder__position').last().questionorder_set.get(theme__id=self.id).position
            else:
                next_position = next_question.order_by('questionorder__position').first().questionorder_set.get(theme__id=self.id).position
        else:
            if before:
                next_position = position - 512
            else:
                next_position = position + 512
        return (position + next_position)/2


    def test_cycle(pk=0):
        if pk == 0:
            Theme.ids_list = []
        if pk in Theme.ids_list:
            return True
        Theme.ids_list.append(pk)
        for enfant in Theme.objects.get(pk=pk).enfants.all():
            if Theme.test_cycle(enfant.id):
                return True
        return False


class LatexModelType(models.TextChoices):
    HEADER = 'HE'
    FEUILLE = 'FE'
    SEANCE = 'SE'
    QUESTION = 'QU'  # pour compiler un object Question
    FIGURE = 'FI'  # pour compiler une question à partir de son code


class VarReplacer(object):

    def _replace_vars(self, code, **kwargs):
        # TODO: add [[QUESTION_1]] [[QUESTION_2]] ...
        """
        Subtistute all the [[vars]] in a LaTeX source, before compilation

        Possibles vars :
        [[QUESTION]]: the question latex code
        [[CLASSE]]: the classe name
        [[PROF_LATEX]]: nom_latex of the teacher
        [[ANNEE_SCO]]: the school year fromated
        [[NFEUILLE]]: the sheet number
        [[NFEUILLE_n]]: the sheet number, encode on n digits with leading 0
        [[NSEANCE]]: the seance number
        [[NSEANCE_n]]: the seance number, encode on n digits with leading 0
        [[NQUESTION]]: the question number, in the seance
        [[NQUESTION_n]]: the question number, in the seance,
            encode on n digits with leading 0
        [[IDQUESTION]] : the question id
        [[IDQUESTION_n]] : the question id, encode on n digits with leading 0
        [[NIVEAU]]: the level name
        [[IMAGES_DIR]]: the images directory AbsolutePath
        [[THUMBS_DIR]]: the thumbs directory AbsolutePath
        [[NOT_FIRST]] something [[END_NOT_FIRST]]: when used with loop
            on Seances or on questions, write 'something' only at
            the non-first loops
        [[ONLY_FIRST]] something [[END_ONLY_FIRST]]: when used with loop on
            Seances or on questions, write 'something' only at the first loop

        Parameters
        ----------
        str : TYPE
            DESCRIPTION.
        classe : TYPE
            DESCRIPTION.
        feuille : TYPE
            DESCRIPTION.
        seance : TYPE
            DESCRIPTION.
        question : TYPE
            DESCRIPTION.

        Returns
        -------
        str : TYPE
            DESCRIPTION.

        """
        # out = inputString;
        # if (sheet == null && seance !=null) {
        #     sheet = seance.getSheet();
        # }
        question = kwargs.get('question')
        seance = kwargs.get('seance')
        feuille = kwargs.get('feuille')
        if seance and not feuille:
            feuille = seance.feuille
            kwargs['feuille'] = feuille
        classe = kwargs.get('classe')
        if feuille and not classe:
            classe = feuille.classe
            kwargs['classe'] = classe

        if code.find('[[ENTETE]]') >= 0:
            code = code.replace('[[ENTETE]]',
                                LatexModel.objects.get(type=LatexModelType.HEADER).get_code(**kwargs))
        # if code.find('[[FIGURE_CODE]]') >= 0:
        #     out = out.replace('[[FIGURE_CODE]]', code)
        # out = out.replace("[[IMAGES_DIR]]",
        #                   LatexModel.userConfig.GetValue('Paths', 'ImagesDir')
        #                   + "/")
        # out = out.replace("[[THUMBS_DIR]]",
        #                   LatexModel.userConfig.GetValue('Paths', 'ThumbsDir')
        #                   + "/")
        classe = kwargs.get('classe')
        if classe:
            code = code.replace("[[CLASSE]]", classe.nom)
            code = code.replace("[[CLASSE_LATEX]]", classe.nom_latex)
            # code = code.replace("[[NIVEAU]]", classe.niveau.label)
            annee_sco = "{}-{}".format(classe.annee, classe.annee + 1)
            code = code.replace("[[ANNEE_SCO]]", annee_sco)
        if feuille:
            code = self._replace_feuille(code, **kwargs)
        if seance:
            code = self._replace_seance(code, **kwargs)

        #         // TODO : add NQUESTION and NQUESTION_n parameters
        # //         if (nquestion != null) {
        # //             out = out.replace("[[NQUESTION]]");
        # //             Pattern nseancePattern = Pattern.compile("\\[\\[NQUESTION_(\\d+)\\]\\]");
        # //             Matcher matcher = nseancePattern.matcher(out);
        # //             while(matcher.find()) {
        # //                 String nbDigits = matcher.group(1);
        # //                 out = out.replace(String.format("[[NQUESTION_%s]]",nbDigits),
        # //                                   String.format(String.format("%%0%sd", nbDigits),
        # //                                         question.getId());
        # //                 matcher = nseancePattern.matcher(out);
        # //             }
        # //         }
        if question:
            code = self._replace_question(code, **kwargs)
        else:
            code = code.replace("[[QUESTION]]", "~")

        first = kwargs.get('first')
        if first is not None:
            if first:
                code = re.sub(
                    "\\[\\[NOT_FIRST\\]\\].*?\\[\\[END_NOT_FIRST\\]\\]",
                    "",
                    code)
                code = re.sub("\\[\\[(END_|)ONLY_FIRST\\]\\]", "", code)
            else:
                code = re.sub("\\[\\[(END_|)NOT_FIRST\\]\\]", "", code)
                code = re.sub(
                    "\\[\\[ONLY_FIRST\\]\\].*?\\[\\[END_ONLY_FIRST\\]\\]",
                    "",
                    code)
        if classe:
            code = code.replace("[[PROF_LATEX]]", classe.enseignant.nom_latex)
        return code

    def _replace_feuille(self, code, **kwargs):
        print('_replace_feuille:', kwargs)
        feuille = kwargs.get('feuille')
        code = code.replace("[[NFEUILLE]]", str(feuille.get_position()))
        match = re.search("\\[\\[NFEUILLE_(\\d+)\\]\\]", code)
        while match is not None:
            ndigits = match.group(1)
            code = code.replace(
                "[[NFEUILLE_{}]]".format(ndigits),
                ("{" + ":0{}d".format(ndigits) + "}")
                    .format(feuille.get_position()))
            match = re.search("\\[\\[NFEUILLE_(\\d+)\\]\\]", code)
        if code.find("[[SEANCES]]") >= 0:
            temp = ''
            seanceModel = self.model.parts.get(type=LatexModelType.SEANCE)
            first = True
            for seance in feuille.seances.all():
                temp += seanceModel.get_code(seance=seance, first=first)
                first = False
            code = code.replace("[[SEANCES]]", temp)
        return code

    def _replace_seance(self, code, **kwargs):
        seance = kwargs.get('seance')
        code = code.replace("[[NSEANCE]]", str(seance.get_position()))
        match = re.search("\\[\\[NSEANCE_(\\d+)\\]\\]", code)
        while match is not None:
            ndigits = match.group(1)
            code = code.replace("[[NSEANCE_{}]]".format(ndigits),
                                ("{" + ":0{}d".format(ndigits) + "}").format(seance.get_position()))
            match = re.search("\\[\\[NSEANCE_(\\d+)\\]\\]", code)
        if code.find("[[QUESTIONS]]") >= 0:
            temp = ''
            seanceModel = self.model.parts.get(type=LatexModelType.QUESTION)
            first = True
            for position in range(1, 6):
                questionPosee = seance.questionsPosees.filter(position=position)
                if questionPosee.count():
                    question = questionPosee.get().question
                else:
                    question = None
                temp += seanceModel.get_code(question=question, first=first)
                first = False
            code = code.replace("[[QUESTIONS]]", temp)
        return code

    @staticmethod
    def _replace_question(code, **kwargs):
        question = kwargs.get('question')
        code = code.replace("[[IDQUESTION]]", str(question.id))
        match = re.search("\\[\\[IDQUESTION_(\\d+)\\]\\]", code)
        while match is not None:
            ndigits = match.group(1)
            id = question.id
            if id is None:
                code = code.replace("[[IDQUESTION_{}]]".format(ndigits),
                                    "{None}")
            else:
                question = question.replace("[[IDQUESTION_{}]]".format(ndigits),
                                            ("{" + ":0{}d".format(ndigits) + "}").format(question.id))
            match = re.search("\\[\\[IDQUESTION_(\\d+)\\]\\]", code)
        return code.replace("[[QUESTION]]", question.code)


class LatexModel(models.Model, VarReplacer):
    objects = None
    label = models.CharField(max_length=50)  # nom du modèle
    description = models.TextField(blank=True, null=True)  # description du modèle
    type = models.CharField(max_length=2,  # à quoi s'applique ce modèle ?
                            choices=LatexModelType.choices)
    file_name =  models.CharField(max_length=255, blank=True, null=True)
    position = models.IntegerField(blank=True, null=True)  # position dans la liste des modèles
    delete_tmp = models.BooleanField(default=True)  # Supprimer les fichiers temporaires après compilation ?

    def get_code(self, **kwargs):
        mainPart = self.parts.get(type=self.type)
        return mainPart.get_code(**kwargs)

    def get_file_name(self, **kwargs):
        return self._replace_vars(self.file_name, **kwargs)

    def compile(self, **kwargs):
        tempdir = tempfile.mkdtemp()
        texfile = os.path.join(str(tempdir), "file.tex")
        pdffile = os.path.join(str(tempdir), "file.pdf")
        with open(texfile, "w") as output:
            code = self.get_code(**kwargs)
            output.write(code)
        compilok = True
        cmd_tex_array = ["pdflatex", "-interaction=nonstopmode", "-output-directory=" + tempdir, texfile]
        # cmd_png_array = ["convert", "-density", "300", "question.pdf", "-quality", "90", temp_dir]
        log = ''
        outfile = kwargs.get('outfile')
        if outfile:
            baseName = kwargs.get('outfile')[0:-4]
        else:
            baseName = self._replace_vars(self.file_name, **kwargs)
            outfile = baseName + '.pdf'
        try:
            # check_call(cmd_tex_array)  # pour afficher la sortie de compilation
            # check_call(cmd_tex_array, stdout=DEVNULL)  # sans affichage
            run(cmd_tex_array, check=True, capture_output=True)
            if self.type == LatexModelType.QUESTION:
                pdf2image.convert_from_path(
                    pdffile,
                    dpi=140,
                    single_file=True,
                    transparent=True,
                    fmt='png',
                    output_folder='static/thumbs/',
                    output_file=baseName)
            else:
                shutil.move(pdffile, 'static/out/' + outfile)
        except CalledProcessError as e:
            compilok = False
            log = e.output
        if self.delete_tmp:
            shutil.rmtree(tempdir)
        return {'status': compilok, 'outfile': outfile, 'latexlog': log}

# Code correspondant à une partie du modèle,
# les différentes parties s'incluent les unes dans les autre
# grâce à un système de balises.
class LatexModelPart(models.Model, VarReplacer):
    model = models.ForeignKey(LatexModel,
                              on_delete=models.CASCADE,
                              related_name='parts')
    type = models.CharField(max_length=2,  # à quoi s'applique cette partie ?
                            choices=LatexModelType.choices)
    code = models.TextField(blank=True, null=True)  # code Latex de cette partie

    def get_code(self, **kwargs):
        return self._replace_vars(self.code, **kwargs)
