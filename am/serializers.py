import os

from django.db.models import Q, Min
from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import CharField, ModelSerializer, ValidationError

from am.models import Theme, Question, Feuille, Seance, QuestionPosee, Classe, QuestionOrder, LatexModel, LatexModelPart


class ClasseSerializer(ModelSerializer):
    class Meta:
        model = Classe
        fields = ['id', 'nom', 'nom_latex', 'annee', 'feuilles']
        read_only_fields = ['id', 'feuilles']

    def validate(self, data):
        user = self.context['request'].user
        if 'id' not in self.initial_data and Classe.objects.filter(enseignant=user, nom=data['nom'],
                                                                   annee=data['annee']):
            raise ValidationError("Le nom de la classe doit être unique pour une année donnée")
        if 'id' in self.initial_data and Classe.objects.exclude(id=self.initial_data['id']).filter(enseignant=user,
                                                                                                   nom=data['nom'],
                                                                                                   annee=data['annee']):
            raise ValidationError("Le nom de la classe doit être unique pour une année donnée")
        return data


class LatexModelPartSerializer(ModelSerializer):
    class Meta:
        model = LatexModelPart
        fields = ['id', 'code', 'type', 'model']


class LatexModelSerializer(ModelSerializer):
    class Meta:
        model = LatexModel
        fields = ['id', 'label', 'description', 'file_name', 'position', 'type', 'parts']


class QuestionOrderSerializer(ModelSerializer):
    class Meta:
        model = QuestionOrder
        fields = ['theme', 'position']


class QuestionSerializer(ModelSerializer):
    editable = SerializerMethodField()
    auteur = SerializerMethodField()
    # to allow themes update :
    themes = QuestionOrderSerializer(source='questionorder_set', many=True)
    thumb_name = CharField(required=False)

    class Meta:
        model = Question
        fields = ['id', 'code', 'themes', 'thumb_name', 'editable', 'avalider', 'validee', 'auteur']
        read_only_fields = ['id', 'editable', 'auteur']

    def get_editable(self, question):
        user = self.context['request'].user
        return question.auteur == user \
               and not question.validee \
               and QuestionPosee.objects.filter(question=question).count() == 0

    def get_used(self, question):
        pass

    def get_themes(self, question):
        themes = question.themes.values('id', 'questionorder__position')
        positions = {}
        for theme in themes:
            positions[theme['id']] = self.initial_data['positions'][theme['id']]
        return positions

    def get_auteur(self, question):
        return question.auteur.username

    def to_internal_value(self, data):
        """ Change values before the validation"""
        # print('to_internal_value: data=', data)
        if 'themes' in data:
            for qOrder in data['themes']:
                if qOrder['position'] is None:
                    list = QuestionOrder.objects.filter(theme=qOrder['theme'])
                    if not list:
                        qOrder['position'] = 0
                    else:
                        qOrder['position'] = list.order_by('position').first().position - 128
        return super().to_internal_value(data)

    def create(self, validated_data):
        questionOrders = validated_data.pop('questionorder_set')
        question = Question.objects.create(**validated_data)
        for questionOrder in questionOrders:
            if not questionOrder['position']:
                questionOrder['position'] = QuestionOrder.objects.filter(theme=questionOrder['theme']).aggregate(Min('position'))['position__min'] - 128
            QuestionOrder.objects.create(question=question, **questionOrder)
        return question

    def update(self, instance, validated_data):
        # print('update:\n\tinstance={} \n\tvalidate_data={}'.format(serializers.serialize('json', [instance]), validated_data))
        if 'questionorder_set' in validated_data:
            QuestionOrder.objects.filter(question=instance.id).delete()
            questionOrders = validated_data.pop('questionorder_set')
            for qo in questionOrders:
                instance.themes.add(qo['theme'], through_defaults={'position': qo['position']})
        instance.code = validated_data.get('code', instance.code)
        instance.thumb_name = validated_data.get('thumb_name', instance.thumb_name)
        instance.avalider = validated_data.get('avalider', instance.avalider)
        instance.validee = validated_data.get('validee', instance.validee)
        instance.save()
        return instance

    def validate(self, data):
        # print('validate: data=', data)
        question = self.instance
        errors = {}
        old_thumb_path = ''
        if 'code' in data:
            if question is not None:
                old_thumb_path = os.path.join('static/thumbs', question.thumb_name)
            if not question or question.code != data['code'] or not os.path.isfile(old_thumb_path):
                question_temp = Question(code=data['code'])
                result = question_temp.compile()
                if result['status']:
                    data['thumb_name'] = result['outfile']
                    if os.path.isfile(old_thumb_path):
                        os.remove(old_thumb_path)
                else:
                    errors['code'] = "Erreur de compilation"
                    errors['latexlog'] = result['latexlog']
        if len(errors) > 0:
            raise ValidationError(errors)
        return data

    def validate_themes(self, value):
        # print('validate_themes: value=', value)
        if not value or len(value) == 0:
            raise ValidationError("Au moins un thème est obligatoire.")
        for theme in value:
            if theme['theme'].enfants.count() != 0:
                raise ValidationError("Une question ne peut être affectée qu'à des sous-thèmes.")
        return value


class QuestionPoseeSerializer(ModelSerializer):
    # id = IntegerField()
    class Meta:
        model = QuestionPosee
        fields = ['id', 'seance', 'position', 'question', 'score']
        # read_only_fields = ['id']

    def validate_seance(self, seance):
        if self.context['request'].user != seance.feuille.classe.enseignant:
            raise ValidationError("Vous n'êtes pas l'enseignant de cette séance.")
        return seance

    def validate_question(self, question):
        if not question.avalider:
            raise ValidationError("On peut pas utiliser une question qui n'est pas 'à valider'.")
        if not question.validee and self.context['request'].user != question.auteur:
            raise ValidationError("On peut pas utiliser une question non validée dont on n'est pas l'auteur.")
        return question


class SeanceSerializer(ModelSerializer):
    """
    Sérializer pour Seance
    """

    class Meta:
        model = Seance
        fields = ['id', 'feuille', 'date', 'position', 'questionsPosees']
        read_only_fields = ['id', 'questionsPosees']

    # def create(self, validated_data):
    #     print('serializer.create')
    #     super().create(validated_data)


class SeanceSerializerR(ModelSerializer):
    """
    Sérializer récursif pour Seance
    """
    questionsPosees = QuestionPoseeSerializer(many=True)

    class Meta:
        model = Seance
        fields = ['id', 'date', 'position', 'questionsPosees', 'feuille']


class FeuilleSerializer(ModelSerializer):
    """
    Sérializer pour Feuille
    """

    class Meta:
        model = Feuille
        fields = ['id', 'classe', 'position', 'seances']
        read_only_fields = ['id']


class ThemeSerializer(ModelSerializer):
    hasQuestion = SerializerMethodField()
    has_question_array = {}

    class Meta:
        model = Theme
        fields = ['id', 'label', 'parent', 'enfants', 'hasQuestion']
        read_only_fields = ['id', 'hasQuestion']

    def get_hasQuestion(self, theme):
        user = self.context['request'].user
        if theme.enfants.count() > 0:
            return None
        else:
            if user.has_perms('am.question_valider'):
                return theme.questions.filter(Q(auteur=user) | Q(avalider=True) | Q(validee=True)).count() > 0
            else:
                return theme.questions.filter(Q(auteur=user) | Q(validee=True)).count() > 0
